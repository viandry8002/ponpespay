// const base_url = 'https://serverwan.com/ponpespay/api/public/api/';
const base_url = 'https://ponpespay.teknologibandung.com/api-ponpes/api/';
export const key = 'base64:9M5f7o/6SD4nD1RxPrdB4OI5YASDGyScT7JFkikUR1D=';

// auth
export const postRegister = base_url + `register`;
export const postLogin = base_url + `login/email`;
// export const postLogout = base_url + `logout`;
export const getMe = base_url + `me`;
export const postUpdateProfile = base_url + `profile/update`;
export const postUpdatePassword = base_url + `profile/update-password`;
export const postForget = base_url + `forgot`;
export const postResendRegist = base_url + `otp/resend/regist`;
export const postResendReset = base_url + `otp/resend/reset`;
export const postCheckOtp = base_url + `otp/verify/regist`;
export const postCheckOtpReset = base_url + `otp/verify/reset`;
export const postResetPassword = base_url + `reset`;

// address
export const getAddress = base_url + `address`;
export const postAddress = base_url + `profile/update-address`;
export const getProvince = base_url + `provinces`;
export const getCity = base_url + `cities/`;
export const getSubDistrict = base_url + `subdistricts/`;

//home
export const getHome = base_url + `home`;
// product
export const getProductsCategories = base_url + `category`;
export const getProducts = base_url + `product`;
export const getProductsDetail = base_url + `products/detail`;

// wallet
export const getWallet = base_url + `wallets/me`;
export const postPayment = base_url + `payment`;
export const postWalletCreate = base_url + `wallets/create`;
export const postWalletHistory = base_url + `wallets/histories`;
export const getWalletHistoryDetail = base_url + `wallets/detail`;

// transaciton
export const getTransactionHistory = base_url + `histories`;
export const getTransactionDetail = base_url + `histories/detail`;
