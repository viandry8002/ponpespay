import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from './Colors';

const Font = StyleSheet.create({
  title32Large: {fontWeight: '600', fontSize: 32, color: Colors.black},
  title30Large: {fontWeight: '600', fontSize: 30, color: Colors.black},
  titleLarge: {fontWeight: '700', fontSize: 18, color: Colors.black},
  titleLarge2: {fontWeight: '400', fontSize: 18, color: Colors.black},
  title: {fontWeight: '700', fontSize: 16, color: Colors.black},
  subtitle: {fontWeight: '700', fontSize: 14, color: Colors.black},
  subtitle2: {fontWeight: '400', fontSize: 14, color: Colors.black},
  bold: {fontWeight: '700', fontSize: 12, color: Colors.black},
  text: {fontWeight: '400', fontSize: 12, color: Colors.black},
  captionBold: {fontWeight: '700', fontSize: 11, color: Colors.black},
  caption: {fontWeight: '400', fontSize: 11, color: Colors.black},
});

export default Font;
