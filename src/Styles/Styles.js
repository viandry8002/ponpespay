import {StyleSheet, Text, View, Dimensions} from 'react-native';
import React from 'react';

export const screenWidth = Dimensions.get('window').width;
export const screenWidthMargin = Dimensions.get('window').width - 32;
export const screenHeight = Dimensions.get('window').height;
