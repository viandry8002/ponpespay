import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import Horizontal from '../../../Compenent/Horizontal';

const ListBank = [
  {
    title: null,
    items: [
      {
        id: 42,
        type: 'bank_transfer',
        code: 'walet',
        title: 'Saldo Dompet',
        description:
          'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis sequi explicabo officiis velit voluptatum consectetur temporibus distinctio? Vel vitae qui provident dolorem distinctio beatae? Eveniet illo tempora ratione possimus ut.',
        file: require('../../../../Assets/icons/Wallet_fill.png'),
        fee_type: 'nominal',
        fee: '4400',
      },
    ],
  },
  {
    title: 'Transfer Virtual Account',
    items: [
      {
        id: 42,
        type: 'bank_transfer',
        code: 'bca',
        title: 'BCA Virtual Account',
        description:
          'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis sequi explicabo officiis velit voluptatum consectetur temporibus distinctio? Vel vitae qui provident dolorem distinctio beatae? Eveniet illo tempora ratione possimus ut.',
        file: require('../../../../Assets/images/payment/bca.png'),
        fee_type: 'nominal',
        fee: '4400',
      },
      {
        id: 45,
        type: 'bank_transfer',
        code: 'mandiri',
        title: 'Mandiri Virtual Account',
        description: null,
        file: require('../../../../Assets/images/payment/mandiri.png'),
        fee_type: 'nominal',
        fee: '1000',
      },
    ],
  },
  {
    title: 'Pembayaran Instant',
    items: [
      {
        id: 26,
        type: 'internet_banking',
        code: 'gopay',
        title: 'Go - PAY',
        description:
          'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis sequi explicabo officiis velit voluptatum consectetur temporibus distinctio? Vel vitae qui provident dolorem distinctio beatae? Eveniet illo tempora ratione possimus ut.',
        file: require('../../../../Assets/images/payment/gopay.png'),
        fee_type: 'nominal',
        fee: '2000',
      },
      {
        id: 27,
        type: 'internet_banking',
        code: 'dana',
        title: 'Dana',
        description:
          'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis sequi explicabo officiis velit voluptatum consectetur temporibus distinctio? Vel vitae qui provident dolorem distinctio beatae? Eveniet illo tempora ratione possimus ut.',
        file: require('../../../../Assets/images/payment/dana.png'),
        fee_type: 'nominal',
        fee: '1000',
      },
    ],
  },
];

const PaymentMethod = ({route, navigation}) => {
  const renderItem = (item, i) => (
    <>
      <TouchableOpacity
        key={i}
        style={styles.wrapItem}
        onPress={() =>
          route.params.type === 'checkoutTu'
            ? navigation.navigate('CheckoutTU', {
                type: route.params.type,
                nominal: route.params.nominal,
                data: route.params.data,
                pMethod: item,
                // carts: route.params.carts,
                // payment: item,
                // voucher: route.params.voucher,
              })
            : false
        }>
        <Image
          source={{uri: item.file}}
          style={styles.imageItem}
          resizeMode={'contain'}
        />
        <Text style={Font.text}>{item.title}</Text>
      </TouchableOpacity>
      <Horizontal />
    </>
  );

  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: Colors.white, paddingHorizontal: 16}}>
      <FlatList
        data={route.params.data}
        renderItem={({item}) => (
          <>
            {item.title ? (
              <Text style={[Font.subtitle, {marginTop: 16}]}>{item.title}</Text>
            ) : (
              false
            )}
            {item.collection.map((dt, i) => renderItem(dt, i))}
          </>
        )}
      />
    </SafeAreaView>
  );
};

export default PaymentMethod;

const styles = StyleSheet.create({
  wrapItem: {flexDirection: 'row', marginVertical: 16, alignItems: 'center'},
  imageItem: {width: 48, height: 32, marginRight: 32},
});
