import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import React, {useEffect, useState, useRef} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Horizontal from '../../Compenent/Horizontal';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {TextMask} from 'react-native-masked-text';
import {screenWidth} from '../../../Styles/Styles';
import BtnOutline from '../../Compenent/BtnOutline';
import FormatMoney from '../../Compenent/FormatMoney';
import BtnPrimary from '../../Compenent/BtnPrimary';
import {Modalize} from 'react-native-modalize';

const Product = [
  {
    id: 1,
    title: 'Teh Pucuk Harum 250ml',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 2,
    title: 'Chitato Sapi Panggang 68gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 3,
    title: 'Indomie Mie Goreng Original 85gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 4,
    title: 'Indomie Mie Goreng Cup 75gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
];

const listCourier = [
  {
    id: 1,
    title: 'JNE - Reguler',
    price: 10000,
  },
  {
    id: 2,
    title: 'Sicepat - Reguler',
    price: 10000,
  },
  {
    id: 3,
    title: 'J&T - Reguler',
    price: 10000,
  },
  {
    id: 4,
    title: 'AnterAja - Reguler',
    price: 10000,
  },
];

const Checkout = ({navigation}) => {
  const modalCourier = useRef(null);

  const onOpen = () => {
    modalCourier.current?.open();
  };

  const onClose = () => {
    modalCourier.current?.close();
  };

  const textRows = (text1, text2, right) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text
        style={[
          Font.bold,
          right === true ? {flex: 1, textAlign: 'right'} : {flex: 1},
        ]}>
        {text2}
      </Text>
    </View>
  );

  const renderPengiriman = ({item}) => (
    <TouchableOpacity>
      <Text style={Font.bold}>{item.title}</Text>
      <FormatMoney value={item.price} style={Font.text} />
      <View style={{height: 5}} />
      <Horizontal />
      <View style={{height: 8}} />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* header */}

        <View style={styles.wrapContent}>
          <Text style={[Font.subtitle]}>Pengiriman</Text>
          <View style={{height: 16}} />
          <TouchableOpacity
            style={styles.wrapPayment}
            onPress={() =>
              navigation.navigate('RegistAddress', {type: 'change'})
            }>
            <View style={styles.rowCenter}>
              <Feather name="map-pin" size={20} color={Colors.primary1} />
              {/* <Text style={[Font.text, {color: Colors.gray2, marginLeft: 8}]}>
                Masukan Alamat
              </Text> */}
              <View style={{flex: 1, marginLeft: 8}}>
                <Text style={Font.bold}>Aditiya Permana</Text>
                <Text style={[Font.text, {color: Colors.gray2}]}>
                  0812932829320
                </Text>
                <Text style={[Font.text, {color: Colors.gray2}]}>
                  Jl. Ciomas Raya Rt01/Rw011 Desa Ciomas Kecamatan Ciomas
                </Text>
              </View>
            </View>
            <AntDesign name="right" size={16} color={Colors.gray2} />
          </TouchableOpacity>
          <View style={{height: 16}} />
          <TouchableOpacity style={styles.wrapPayment} onPress={() => onOpen()}>
            <View style={styles.rowCenter}>
              <MaterialCommunityIcons
                name="truck-fast-outline"
                size={20}
                color={Colors.primary1}
              />
              {/* <Text style={[Font.text, {color: Colors.gray2, marginLeft: 8}]}>
                Pilih Pengiriman
              </Text> */}
              <View style={{flex: 1, marginLeft: 8}}>
                <Text style={Font.bold}>JNE - Reguler</Text>
                <FormatMoney
                  style={[Font.text, {color: Colors.gray2}]}
                  value={10000}
                />
              </View>
            </View>
            <AntDesign name="right" size={16} color={Colors.gray2} />
          </TouchableOpacity>

          {/* Rangkuman Pesanan */}
          <View style={{height: 16}} />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={[Font.subtitle]}>Rangkuman Pesanan</Text>
            <Text style={[Font.bold, {color: Colors.primary1}]}>
              Tambah Pesanan
            </Text>
          </View>
          <FlatList
            data={Product}
            renderItem={({item}) => (
              <>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginBottom: 16,
                    alignItems: 'center',
                  }}>
                  <View style={styles.wrapQtyProduct}>
                    <Text style={[Font.bold, {color: Colors.primary1}]}>
                      {item.qty}
                    </Text>
                  </View>
                  <View style={{flex: 1, marginHorizontal: 16}}>
                    <Text style={[Font.text, {color: Colors.gray1}]}>
                      {item.title}
                    </Text>
                    <Text style={[Font.bold, {color: Colors.primary1}]}>
                      Edit
                    </Text>
                  </View>
                  <FormatMoney value={item.total} style={[Font.bold]} />
                </View>
                <Horizontal />
                <View style={{height: 16}} />
              </>
            )}
            style={{marginTop: 16}}
          />
          <TouchableOpacity
            style={styles.wrapPayment}
            onPress={() => navigation.navigate('PaymentMethod')}>
            {/* <View style={styles.rowCenter}>
              <Text
                style={[
                  Font.text,
                  {color: Colors.gray2, marginLeft: 8, flex: 1},
                ]}>
                Pilih Pembayaran
              </Text>
              <Text
                style={[Font.text, {color: Colors.primary1, marginLeft: 8}]}>
                Pilih Metode Pembayaran
              </Text>
              <AntDesign name="right" size={16} color={Colors.primary1} />
            </View> */}
            <View style={styles.rowCenter}>
              <View style={styles.rowCenter}>
                <Image
                  source={require('../../../Assets/images/payment/bca.png')}
                  style={{width: 48, height: 32, resizeMode: 'contain'}}
                />
                <Text
                  style={[
                    Font.text,
                    {color: Colors.gray2, marginLeft: 8, flex: 1},
                  ]}>
                  BCA Virtual Account
                </Text>
              </View>
              <AntDesign name="right" size={16} color={Colors.placeholder} />
            </View>
          </TouchableOpacity>
          <View style={{height: 16}} />
          <Text style={[Font.subtitle]}>Rincian Pesanan</Text>
          <View style={{height: 16}} />
          {textRows('Sub Total', <FormatMoney value={'1200000'} />, true)}
          {textRows(
            'Ongkos Kirim',
            <FormatMoney value={'5000'} style={[Font.text]} />,
            true,
          )}
        </View>
      </ScrollView>
      <View style={styles.absoluteBottom}>
        {textRows(
          <Text style={[Font.text]}>Total Harga</Text>,
          <FormatMoney value={'1200000'} style={[Font.titleLarge]} />,
          true,
        )}
        <BtnPrimary
          title={'Pesan'}
          pressed={() => navigation.navigate('SuccessCheckoutProduct')}
        />
      </View>
      {/* modalize Domisili */}
      <Modalize ref={modalCourier} handlePosition={'inside'} modalHeight={400}>
        <View style={{padding: 16}}>
          <View style={styles.wrapDetailOrder}>
            <Text style={Font.subtitle}>Pilih Kurir</Text>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={Colors.black}
              onPress={() => onClose(modalCourier)}
            />
          </View>

          <FlatList
            showsVerticalScrollIndicator={false}
            data={listCourier}
            renderItem={renderPengiriman}
            // ListEmptyComponent={
            //   <View
            //     style={{
            //       flex: 1,
            //       justifyContent: 'center',
            //       alignItems: 'center',
            //     }}>
            //     <Text style={styles.subtitle}>{'tunggu sebentar..'}</Text>
            //   </View>
            // }
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default Checkout;

const styles = StyleSheet.create({
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  spaceGrey: {backgroundColor: Colors.container, height: 8},
  spacing: {height: 16},
  wrapContent: {backgroundColor: Colors.white, flex: 1, padding: 16},
  wrapPayment: {
    width: screenWidth - 32,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.container,
    paddingVertical: 12,
    paddingHorizontal: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapQtyProduct: {
    width: 36,
    padding: 8,
    borderWidth: 1,
    borderColor: Colors.gray5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 100,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
  rowCenter: {flexDirection: 'row', alignItems: 'center', flex: 1},
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
});
