import React, {useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {screenWidth} from '../../../../Styles/Styles';
import BtnOutline from '../../../Compenent/BtnOutline';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import Horizontal from '../../../Compenent/Horizontal';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FormatMoney from '../../../Compenent/FormatMoney';
import Gradient from '../../../Compenent/Gradient';

const Product = [
  {
    id: 1,
    title: 'Teh Pucuk Harum 250ml',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 2,
    title: 'Chitato Sapi Panggang 68gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 3,
    title: 'Indomie Mie Goreng Original 85gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 4,
    title: 'Indomie Mie Goreng Cup 75gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
];

const SuccessCheckoutProduct = ({route}) => {
  useEffect(() => {}, []);

  const textRows = (text1, text2, right) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text
        style={[
          Font.bold,
          right === true ? {flex: 1, textAlign: 'right'} : {flex: 1},
        ]}>
        {text2}
      </Text>
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Gradient
          style={{height: 112, width: screenWidth, position: 'absolute'}}
        />
        {/* absolute modal */}
        <View style={styles.wrapAbsoluteModal}>
          <Image
            source={require('../../../../Assets/icons/success.png')}
            style={styles.imageAbsolute}
            resizeMode={'contain'}
          />
          <Text
            style={[
              Font.titleLarge,
              {
                color: Colors.success,
                textAlign: 'center',
                marginTop: 20,
                marginBottom: 16,
              },
            ]}>
            Transaksi Berhasil Dibuat
          </Text>
          {textRows(
            'Status',
            <View
              style={{
                backgroundColor: Colors.primary1shading,
                borderRadius: 8,
                padding: 8,
              }}>
              <Text style={[Font.bold, {color: Colors.primary1}]}>
                Diproses
              </Text>
            </View>,
          )}
          {textRows('Waktu Pembelian', '12 Des 2021 , 09:20')}
          {textRows(
            'Invoice',
            <TouchableOpacity style={styles.wrapCopy}>
              <Text style={Font.bold}>102381082421 </Text>
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>,
          )}
        </View>
        {/* Pembayaran */}
        <View style={{margin: 16}}>
          <Text style={[Font.subtitle]}>Pembayaran</Text>
          <View style={{flex: 1, height: 16}} />
          <View
            style={{
              width: screenWidth - 32,
              borderWidth: 1,
              borderRadius: 8,
              borderColor: Colors.container,
              padding: 18,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../../../../Assets/images/bca.png')}
                style={{width: 48, height: 16, marginRight: 8}}
              />
              <Text style={Font.text}>BCA Virtual Account</Text>
            </View>
            <View style={styles.spacing} />
            <Horizontal />
            <View style={styles.spacing} />
            <View>
              <Text style={[Font.text, {colo: Colors.placeholder}]}>
                No Virtual Account
              </Text>
              <View style={{height: 4}} />
              <TouchableOpacity
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={Font.title}>32323232332323 </Text>
                <Feather name="copy" size={18} color={Colors.black} />
              </TouchableOpacity>
            </View>
            <View style={styles.spacing} />
            <Horizontal />
            <View style={styles.spacing} />
            <Text style={[Font.text, {colo: Colors.placeholder}]}>
              Nominal Pembayaran
            </Text>
            <View style={{height: 4}} />
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={Font.title}>Rp10.000.000 </Text>
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>
            <View style={styles.spacing} />
            <BtnOutline title={'Cara Pembayaran'} />
          </View>

          {/* Rangkuman Pesanan */}
          <View style={styles.spaceGrey} />
          <View style={styles.wrapContent}>
            <View style={styles.spacing} />
            <Text style={[Font.subtitle]}>Rangkuman Pesanan</Text>
            <FlatList
              data={Product}
              renderItem={({item}) => (
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginBottom: 16,
                    alignItems: 'center',
                  }}>
                  <View style={styles.wrapQtyProduct}>
                    <Text style={[Font.bold, {color: Colors.primary1}]}>
                      {item.qty}
                    </Text>
                  </View>
                  <View style={{flex: 1, marginHorizontal: 16}}>
                    <Text style={[Font.text, {color: Colors.gray1}]}>
                      {item.title}
                    </Text>
                    <Text style={[Font.text, {color: Colors.placeholder}]}>
                      {item.qty} x {item.price}
                    </Text>
                  </View>
                  <FormatMoney value={item.total} style={[Font.bold]} />
                </View>
              )}
              style={{marginTop: 16}}
            />
            <Text style={[Font.subtitle]}>Rangkuman Pesanan</Text>

            <View style={styles.spacing} />
            {textRows('Sub Total', 'Rp1.200.000', true)}
            {textRows('Ongkos Kirim', 'Rp5.000', true)}
            {textRows(
              'Total Harga',
              <FormatMoney value={1205000} style={[Font.titleLarge]} />,
              true,
            )}
            <Horizontal />
            <View style={styles.spacing} />
            <BtnPrimary title={'Kembali Ke Beranda'} />
            <View style={styles.spacing} />
            <BtnOutline
              title={'Hubungi Kami'}
              color={Colors.gray2}
              leftIcon={
                <SimpleLineIcons
                  name="phone"
                  size={18}
                  color={Colors.gray2}
                  style={{marginRight: 10}}
                />
              }
            />
            <View style={styles.spacing} />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default SuccessCheckoutProduct;

const styles = StyleSheet.create({
  wrapAbsoluteModal: {
    marginTop: 39,
    marginHorizontal: 16,
    borderRadius: 8,
    width: screenWidth - 32,
    padding: 16,
    backgroundColor: Colors.white,
    elevation: 10,
  },
  imageAbsolute: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    elevation: 20,
    position: 'absolute',
    top: -30,
  },
  spacing: {height: 16},
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  wrapCopy: {flexDirection: 'row', alignItems: 'center', flex: 1},
  wrapContent: {backgroundColor: Colors.white, flex: 1},
  wrapPayment: {
    width: screenWidth - 32,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.container,
    paddingVertical: 12,
    paddingHorizontal: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapQtyProduct: {
    width: 36,
    padding: 8,
    borderWidth: 1,
    borderColor: Colors.gray5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 100,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
  rowCenter: {flexDirection: 'row', alignItems: 'center', flex: 1},
});
