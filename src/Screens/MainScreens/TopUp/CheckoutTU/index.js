import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import FormInput from '../../../Compenent/FormInput';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FormatMoney from '../../../Compenent/FormatMoney';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  key,
  postPayment,
  postWalletCreate,
} from '../../../../Variable/ApiClient';
import AlertMsg from '../../../Compenent/AlertMsg';
import SpinnerLoad from '../../../Compenent/SpinnerLoad';

const CheckoutTU = ({route, navigation}) => {
  const [token, setToken] = useState('');
  const [load, setLoad] = useState(false);

  const getToken = async () => {
    try {
      const auth = await AsyncStorage.getItem('api_token');
      return setToken(auth);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const getListPayment = () => {
    setLoad(true);
    axios
      .post(
        postPayment,
        {type: 'topup', nominal: route.params.nominal.title},
        {
          headers: {
            key: key,
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then(res => {
        setLoad(false);
        res.data.success
          ? navigation.navigate('PaymentMethod', {
              type: 'checkoutTu',
              nominal: {title: route.params.nominal.title},
              data: res.data.data,
            })
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        setLoad(false);
        console.log(err);
      });
  };

  const submit = () => {
    setLoad(true);
    axios
      .post(
        postWalletCreate,
        {
          nominal: route.params.nominal.title,
          payment_method: route.params.pMethod.id,
        },
        {
          headers: {
            key: key,
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then(res => {
        console.log('gagagah', res.data.data);
        setLoad(false);
        res.data.success
          ? navigation.navigate('SuccessTU', {data: res.data.data})
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        setLoad(false);
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <SpinnerLoad loads={load} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{flex: 1, margin: 16}}>
          <Text style={[Font.subtitle]}>Jumlah Top Up</Text>
          <FormInput
            lefticon={<Text style={Font.titleLarge}>Rp</Text>}
            icon={<View />}
            val={route.params.nominal.title.toString()}
            styled={[Font.titleLarge]}
            disabled={true}
            // change={value => onInputChange(value, 'username')}
          />
          <Text style={[Font.subtitle]}>Metode Pembayaran</Text>
          <View style={{height: 16}} />
          {/* payment method */}
          <TouchableOpacity
            style={styles.wrapPayment}
            onPress={() => getListPayment()}>
            {route.params.pMethod ? (
              <View style={styles.rowCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    source={{uri: route.params.pMethod.img_url}}
                    style={{width: 48, height: 32, resizeMode: 'contain'}}
                  />
                  <Text
                    style={[
                      Font.text,
                      {color: Colors.gray2, marginLeft: 8, flex: 1},
                    ]}>
                    {route.params.pMethod.title}
                  </Text>
                </View>
                <AntDesign name="right" size={16} color={Colors.placeholder} />
              </View>
            ) : (
              <View style={styles.rowCenter}>
                <Text style={[Font.text, {color: Colors.gray2, flex: 1}]}>
                  Pilih Pembayaran
                </Text>
                <Text
                  style={[Font.text, {color: Colors.primary1, marginLeft: 8}]}>
                  Pilih Metode Pembayaran
                </Text>
                <AntDesign name="right" size={16} color={Colors.primary1} />
              </View>
            )}
          </TouchableOpacity>
          <View style={{height: 16}} />
          <Text style={[Font.subtitle]}>Rincian Transaksi</Text>
          <View style={styles.wrapTextRow}>
            <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
              Nominal
            </Text>
            <FormatMoney
              value={route.params.nominal.title}
              style={[Font.bold, {flex: 1}]}
            />
          </View>
          <View style={styles.wrapTextRow}>
            <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
              Biaya Admin
            </Text>
            <Text style={[Font.bold, {flex: 1}]}>Rp0</Text>
          </View>
          <View style={styles.wrapTextRow}>
            <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
              Total Transaksi
            </Text>
            <Text style={[Font.title, {flex: 1}]}>Rp10.000.000</Text>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          backgroundColor: Colors.white,
          height: 72,
          paddingHorizontal: 16,
          justifyContent: 'center',
          elevation: 10,
        }}>
        <BtnPrimary title={'Lanjut'} pressed={() => submit()} />
      </View>
    </SafeAreaView>
  );
};

export default CheckoutTU;

const styles = StyleSheet.create({
  wrapTextRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 16,
  },
  wrapPayment: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.container,
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rowCenter: {flexDirection: 'row', alignItems: 'center', flex: 1},
});
