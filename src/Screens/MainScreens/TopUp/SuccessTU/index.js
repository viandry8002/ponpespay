import moment from 'moment';
import React, {useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {screenWidth} from '../../../../Styles/Styles';
import BtnOutline from '../../../Compenent/BtnOutline';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import FormatMoney from '../../../Compenent/FormatMoney';
import Horizontal from '../../../Compenent/Horizontal';

const SuccessTU = ({route, navigation}) => {
  useEffect(() => {}, []);

  const textRows = (text1, text2) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text style={[Font.bold, {flex: 1}]}>{text2}</Text>
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <LinearGradient
          colors={[Colors.primary1, Colors.primary2]}
          start={{x: 0.0, y: 0.5}}
          end={{x: 1.0, y: 0.5}}
          style={{height: 112, width: screenWidth, position: 'absolute'}}
        />
        {/* absolute modal */}
        <View style={styles.wrapAbsoluteModal}>
          <Image
            source={require('../../../../Assets/icons/success.png')}
            style={styles.imageAbsolute}
            resizeMode={'contain'}
          />
          <Text
            style={[
              Font.titleLarge,
              {
                color: Colors.success,
                textAlign: 'center',
                marginTop: 20,
                marginBottom: 16,
              },
            ]}>
            Transaksi Berhasil Dibuat
          </Text>
          {textRows(
            'Status',
            <View
              style={{
                backgroundColor: Colors.primary1shading,
                borderRadius: 8,
                padding: 8,
              }}>
              <Text style={[Font.bold, {color: Colors.primary1}]}>
                {route.params.data.label_status}
              </Text>
            </View>,
          )}
          {textRows(
            'Waktu Pembelian',
            moment(route.params.data.created_at).format('DD MMM YYYY, hh:mm'),
          )}
          {textRows(
            'Invoice',
            <TouchableOpacity style={styles.wrapCopy}>
              <Text style={Font.bold}>{route.params.data.code} </Text>
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>,
          )}
          {textRows(
            'Total Transaksi',
            <FormatMoney value={route.params.data.total} />,
          )}
          <View style={{height: 8}} />
        </View>
        {/* Pembayaran */}
        <View style={{margin: 16}}>
          <Text style={[Font.subtitle]}>Pembayaran</Text>
          <View style={{flex: 1, height: 16}} />
          <View
            style={{
              width: screenWidth - 32,
              borderWidth: 1,
              borderRadius: 8,
              borderColor: Colors.container,
              padding: 18,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../../../../Assets/images/bca.png')}
                style={{width: 48, height: 16, marginRight: 8}}
              />
              <Text style={Font.text}>
                {route.params.data.req_payment.title}
              </Text>
            </View>
            <View style={styles.spacing} />
            <Horizontal />
            <View style={styles.spacing} />
            <View>
              <Text style={[Font.text, {colo: Colors.placeholder}]}>
                No Virtual Account
              </Text>
              <View style={{height: 4}} />
              <TouchableOpacity
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={Font.title}>32323232332323 </Text>
                <Feather name="copy" size={18} color={Colors.black} />
              </TouchableOpacity>
            </View>
            <View style={styles.spacing} />
            <Horizontal />
            <View style={styles.spacing} />
            <Text style={[Font.text, {colo: Colors.placeholder}]}>
              Nominal Pembayaran
            </Text>
            <View style={{height: 4}} />
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <FormatMoney
                value={route.params.data.total}
                style={[Font.title, {marginRight: 4}]}
              />
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>
            <View style={styles.spacing} />
            <BtnOutline title={'Cara Pembayaran'} />
          </View>
          {/* Rincian Transaksi */}
          <View style={styles.spacing} />
          <Text style={[Font.subtitle]}>Rincian Transaksi</Text>
          <View style={styles.spacing} />
          {textRows(
            'Nominal',
            <FormatMoney value={route.params.data.subtotal} />,
          )}
          {textRows(
            'Biaya Admin',
            <FormatMoney value={route.params.data.total_fee} />,
          )}
          <Horizontal />
          <View style={styles.spacing} />
          {textRows(
            'Total Transaksi',
            <FormatMoney
              value={route.params.data.total}
              style={[Font.title]}
            />,
          )}
          <BtnPrimary
            title={'Cek History'}
            pressed={() => navigation.navigate('HistoryTU')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default SuccessTU;

const styles = StyleSheet.create({
  wrapAbsoluteModal: {
    marginTop: 39,
    marginHorizontal: 16,
    borderRadius: 8,
    width: screenWidth - 32,
    padding: 16,
    backgroundColor: Colors.white,
    elevation: 10,
  },
  imageAbsolute: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    elevation: 20,
    position: 'absolute',
    top: -30,
  },
  spacing: {height: 16},
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  wrapCopy: {flexDirection: 'row', alignItems: 'center', flex: 1},
});
