import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import FormInput from '../../Compenent/FormInput';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FormatMoney from '../../Compenent/FormatMoney';

const Icon2 = [
  {
    id: 1,
    title: 20000,
    image: require('../../../Assets/images/money/1.png'),
  },
  {
    id: 2,
    title: 50000,
    image: require('../../../Assets/images/money/2.png'),
  },
  {
    id: 3,
    title: 100000,
    image: require('../../../Assets/images/money/3.png'),
  },
  {
    id: 4,
    title: 200000,
    image: require('../../../Assets/images/money/4.png'),
  },
  {
    id: 5,
    title: 500000,
    image: require('../../../Assets/images/money/5.png'),
  },
  {
    id: 6,
    title: 1000000,
    image: require('../../../Assets/images/money/6.png'),
  },
];

const TopUp = ({navigation}) => {
  const [chosed, setChosed] = useState({title: 0});

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={[Font.subtitle, {margin: 16}]}>Pilih nominal top up</Text>
        <View style={styles.rowMargin}>
          {Icon2.map((data, i) =>
            data.title === '' && data.image === '' ? (
              <View style={{flex: 1}} key={i} />
            ) : (
              <TouchableOpacity
                key={i}
                style={
                  chosed.id === data.id
                    ? styles.centerFlexChosed
                    : styles.centerFlex
                }
                onPress={() =>
                  chosed.id === data.id
                    ? setChosed({title: 0})
                    : setChosed(data)
                }>
                <Image source={data.image} style={{width: 56, height: 56}} />
                <View style={{height: 8}} />
                <FormatMoney value={data.title} style={Font.subtitle} />
              </TouchableOpacity>
            ),
          )}
        </View>
        <View style={{flex: 1, marginHorizontal: 16}}>
          <Text style={[Font.subtitle]}>Atau Isi Nominal Sendiri</Text>
          <FormInput
            // title={'Email'}
            placholder={'Masukan Nominal Top Up'}
            lefticon={<Text style={Font.titleLarge}>Rp</Text>}
            icon={<View />}
            styled={chosed?.title > 0 ? Font.titleLarge : ''}
            val={chosed?.title === 0 ? '' : chosed?.title.toString()}
            change={value => setChosed({title: value})}
            errorMsg=" Minimal Top Up Rp20.000"
            errorMsgStyle={[Font.bold, {color: Colors.placeholder}]}
            tkeyboard={'numeric'}
          />
        </View>
      </ScrollView>
      <View
        style={{
          backgroundColor: Colors.white,
          height: 72,
          paddingHorizontal: 16,
          justifyContent: 'center',
          elevation: 10,
        }}>
        <BtnPrimary
          title={'Lanjut'}
          pressed={() => navigation.navigate('CheckoutTU', {nominal: chosed})}
        />
      </View>
    </SafeAreaView>
  );
};

export default TopUp;

const styles = StyleSheet.create({
  rowMargin: {
    marginBottom: 16,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  centerFlexChosed: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 110,
    height: 112,
    margin: 8,
    borderWidth: 1,
    backgroundColor: Colors.primary1shading,
    borderColor: Colors.primary1,
    borderRadius: 8,
  },
  centerFlex: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 110,
    height: 112,
    margin: 8,
    borderWidth: 1,
    borderColor: Colors.container,
    borderRadius: 8,
  },
});
