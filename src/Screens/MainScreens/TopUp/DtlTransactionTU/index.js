import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import Foundation from 'react-native-vector-icons/Foundation';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {screenWidth} from '../../../../Styles/Styles';
import {key, getWalletHistoryDetail} from '../../../../Variable/ApiClient';
import BtnOutline from '../../../Compenent/BtnOutline';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import FormatMoney from '../../../Compenent/FormatMoney';
import Horizontal from '../../../Compenent/Horizontal';

const DtlTransactionTU = ({route, navigation}) => {
  const [token, setToken] = useState('');
  const [load, setLoad] = useState(false);
  const [cmpStatus, setCmpStatus] = useState();
  const [data, setData] = useState([]);
  const [dataPayment, setDataPayment] = useState([]);

  const getToken = async () => {
    try {
      const auth = await AsyncStorage.getItem('api_token');
      return getDtlHistory(auth);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const getDtlHistory = auth => {
    // setLoad(true);
    setToken(auth);
    axios
      .get(getWalletHistoryDetail + '/' + route.params.id, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        // setLoad(false);
        res.data.success
          ? (setData(res.data.data),
            setDataPayment(res.data.data.req_payment),
            componentStatus(res.data.data.label_status))
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        // setLoad(false);
        console.log(err);
      });
  };

  const componentStatus = status => {
    switch (status) {
      case 'Menunggu Pembayaran':
        return setCmpStatus({
          title: 'Transaksi Menunggu Pembayaran',
          status: status,
          color: Colors.yellow,
          colorShading: Colors.yellowshading,
          icon: require('../../../../Assets/icons/process2.png'),
        });
      case 'Diproses':
        return setCmpStatus({
          title: 'Transaksi Sedang Diproses',
          status: status,
          color: Colors.primary1,
          colorShading: Colors.primary1shading,
          icon: require('../../../../Assets/icons/process.png'),
        });
      case 'Selesai':
        return setCmpStatus({
          title: 'Transaksi Berhasil',
          status: status,
          color: Colors.success,
          colorShading: Colors.successshading,
          icon: require('../../../../Assets/icons/success.png'),
        });
      case 'Gagal':
        return setCmpStatus({
          title: 'Transaksi Gagal',
          status: status,
          color: Colors.error,
          colorShading: Colors.errorshading,
          icon: require('../../../../Assets/icons/error.png'),
        });
    }
  };

  const textRows = (text1, text2, rightCustom) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      {text2 == '' ? (
        rightCustom
      ) : (
        <Text style={[Font.bold, {flex: 1}]}>{text2}</Text>
      )}
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <LinearGradient
          colors={[Colors.primary1, Colors.primary2]}
          start={{x: 0.0, y: 0.5}}
          end={{x: 1.0, y: 0.5}}
          style={{height: 112, width: screenWidth, position: 'absolute'}}
        />
        {/* absolute modal */}
        <View style={styles.wrapAbsoluteModal}>
          <Image
            source={cmpStatus?.icon}
            style={styles.imageAbsolute}
            resizeMode={'contain'}
          />
          <Text
            style={[
              Font.titleLarge,
              {
                color: cmpStatus?.color,
                textAlign: 'center',
                marginTop: 20,
                marginBottom: 16,
              },
            ]}>
            Transaksi {data?.label_status}
          </Text>
          {route.params.type === 'Gagal' ? (
            <View style={styles.wrapError}>
              <Foundation name="info" size={21} color={Colors.error} />
              <Text style={[Font.text, {marginLeft: 12}]}>
                Sedang terjadi gangguan, sehingga transaksi tidak bisa
                dilanjutkan
              </Text>
            </View>
          ) : (
            false
          )}
          {textRows(
            'Status',
            '',
            <View style={{flex: 1, alignItems: 'flex-start'}}>
              <View
                style={{
                  backgroundColor: cmpStatus?.colorShading,
                  borderRadius: 8,
                  padding: 8,
                }}>
                <Text style={[Font.bold, {color: cmpStatus?.color}]}>
                  {cmpStatus?.status}
                </Text>
              </View>
            </View>,
          )}
          {textRows('Waktu Pembelian', data.created_at)}
          {textRows(
            'Invoice',
            '',
            <TouchableOpacity style={styles.wrapCopy}>
              <Text style={Font.bold}>{data.code} </Text>
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>,
          )}
          {textRows(
            'Total Transaksi',
            <FormatMoney value={data.total} style={Font.bold} />,
          )}
          <View style={{height: 8}} />
        </View>
        <View style={{margin: 16}}>
          {/* Pembayaran */}
          {data.label_status === 'Diproses' ||
          data.label_status === 'Menunggu Pembayaran' ? (
            <View>
              <Text style={[Font.subtitle]}>Pembayaran</Text>
              <View style={{flex: 1, height: 16}} />
              <View
                style={{
                  width: screenWidth - 32,
                  borderWidth: 1,
                  borderRadius: 8,
                  borderColor: Colors.container,
                  padding: 18,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={{url: dataPayment.img_url}}
                    style={{width: 48, height: 16, marginRight: 8}}
                  />
                  <Text style={Font.text}>{dataPayment.title}</Text>
                </View>
                <View style={styles.spacing} />
                <Horizontal />
                <View style={styles.spacing} />
                <View>
                  <Text style={[Font.text, {color: Colors.placeholder}]}>
                    No Virtual Account
                  </Text>
                  <View style={{height: 4}} />
                  <TouchableOpacity
                    style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={Font.title}>32323232332323 </Text>
                    <Feather name="copy" size={18} color={Colors.black} />
                  </TouchableOpacity>
                </View>
                <View style={styles.spacing} />
                <Horizontal />
                <View style={styles.spacing} />
                <Text style={[Font.text, {colo: Colors.placeholder}]}>
                  Nominal Pembayaran
                </Text>
                <View style={{height: 4}} />
                <TouchableOpacity
                  style={{flexDirection: 'row', alignItems: 'center'}}>
                  <FormatMoney value={data.total} style={Font.title} />
                  <Feather name="copy" size={18} color={Colors.black} />
                </TouchableOpacity>
                <View style={styles.spacing} />
                <BtnOutline title={'Cara Pembayaran'} />
              </View>
              <View style={styles.spacing} />
            </View>
          ) : (
            false
          )}

          {/* Rincian Transaksi */}
          <Text style={[Font.subtitle]}>Rincian Transaksi</Text>
          <View style={styles.spacing} />
          {textRows(
            'Nominal',
            <FormatMoney value={data.subtotal} style={Font.title} />,
          )}
          {textRows(
            'Biaya Admin',
            <FormatMoney value={data.total_fee} style={Font.title} />,
          )}
          <Horizontal />
          <View style={styles.spacing} />
          {textRows(
            'Total Transaksi',
            '',
            <View style={{flex: 1, alignItems: 'flex-start'}}>
              <FormatMoney value={data.total} style={Font.title} />
            </View>,
          )}
          {route.params.type === 'Gagal' ? (
            <BtnPrimary title={'Top Up Ulang'} />
          ) : (
            false
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DtlTransactionTU;

const styles = StyleSheet.create({
  wrapAbsoluteModal: {
    marginTop: 39,
    marginHorizontal: 16,
    borderRadius: 8,
    width: screenWidth - 32,
    padding: 16,
    backgroundColor: Colors.white,
    elevation: 10,
  },
  imageAbsolute: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    // elevation: 20,
    position: 'absolute',
    top: -30,
  },
  spacing: {height: 16},
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  wrapCopy: {flexDirection: 'row', alignItems: 'center', flex: 1},
  wrapError: {
    flexDirection: 'row',
    backgroundColor: Colors.errorshading,
    borderWidth: 1,
    borderColor: Colors.error,
    padding: 12,
    marginBottom: 16,
    borderRadius: 7,
  },
});
