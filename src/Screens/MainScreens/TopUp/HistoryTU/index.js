import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import moment from 'moment';
import React, {useEffect, useRef, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Modalize} from 'react-native-modalize';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {key, postWalletHistory} from '../../../../Variable/ApiClient';
import BtnOutline from '../../../Compenent/BtnOutline';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import FormatMoney from '../../../Compenent/FormatMoney';
import FormInput from '../../../Compenent/FormInput';
import Horizontal from '../../../Compenent/Horizontal';

const riwayatTu = [
  {
    title: 'Top Up Saldo',
    bank: 'BCA Virtual Account',
    price: 'Rp10.000.000',
    status: 'Diproses',
    create_at: 'Sel, 3 Jan 2023, 19:30',
  },
  {
    title: 'Top Up Saldo',
    bank: 'BCA Virtual Account',
    price: 'Rp10.000.000',
    status: 'Selesai',
    create_at: 'Sel, 3 Jan 2023, 19:30',
  },
  {
    title: 'Top Up Saldo',
    bank: 'BCA Virtual Account',
    price: 'Rp10.000.000',
    status: 'Gagal',
    create_at: 'Sel, 3 Jan 2023, 19:30',
  },
  {
    title: 'Top Up Saldo',
    bank: 'BCA Virtual Account',
    price: 'Rp10.000.000',
    status: 'Diproses',
    create_at: 'Sel, 3 Jan 2023, 19:30',
  },
  {
    title: 'Top Up Saldo',
    bank: 'BCA Virtual Account',
    price: 'Rp10.000.000',
    status: 'Selesai',
    create_at: 'Sel, 3 Jan 2023, 19:30',
  },
  {
    title: 'Top Up Saldo',
    bank: 'BCA Virtual Account',
    price: 'Rp10.000.000',
    status: 'Gagal',
    create_at: 'Sel, 3 Jan 2023, 19:30',
  },
];

const filterDate = [
  {
    title: '7 Hari Terakhir',
    desc: 'Min, 1 Jan 2023 - Sab, 7 Jan 2023',
  },
  {
    title: '30 Hari Terakhir',
    desc: 'Min, 1 Jan 2023 - Sab, 7 Jan 2023',
  },
  {
    title: '90 Hari Terakhir',
    desc: 'Min, 1 Jan 2023 - Sab, 7 Jan 2023',
  },
  {
    title: 'Atur Tanggal',
    desc: null,
  },
];

const HistoryTU = ({navigation}) => {
  const modalFilter = useRef(null);

  const onOpen = () => {
    modalFilter.current?.open();
  };

  const onClose = () => {
    modalFilter.current?.close();
  };

  const [token, setToken] = useState('');
  const [load, setLoad] = useState(false);
  const [data, setData] = useState([]);

  const getToken = async () => {
    try {
      const auth = await AsyncStorage.getItem('api_token');
      return getHistory(auth);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const getHistory = auth => {
    // setLoad(true);
    setToken(auth);
    axios
      .post(
        postWalletHistory,
        {start: '', end: ''},
        {
          headers: {
            key: key,
            Authorization: 'Bearer ' + auth,
            // Authorization: 'Bearer ' + auth ? {auth} : {token},
          },
        },
      )
      .then(res => {
        res.data.success
          ? setData(res.data.data.data)
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        // setLoad(false);
        console.log(err);
      });
  };

  const colorStatus = status => {
    switch (status) {
      case 'unpaid':
        return Colors.yellow;
      case 'Diproses':
        return Colors.primary1;
      case 'Selesai':
        return Colors.success;
      case 'Gagal':
        return Colors.error;
    }
  };

  const renderListHistory = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          marginHorizontal: 16,
        }}
        onPress={() => navigation.navigate('DtlTransactionTU', {id: item.id})}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={styles.wrapPlusIcon}>
            <AntDesign name="pluscircle" size={24} color={Colors.primary1} />
          </View>
          <View style={{flex: 1, marginLeft: 8}}>
            <Text style={[Font.subtitle]}>Top Up Saldo</Text>
            <View style={styles.vSpacing8} />
            <Text style={[Font.text]}>{item.req_payment.title}</Text>
            <View style={styles.vSpacing8} />
            <Text style={[Font.caption, {color: Colors.placeholder}]}>
              {moment(item.create_at).format('ddd,DD MMM YYYY, hh:mm')}
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <FormatMoney value={item.total} style={[Font.subtitle]} />
            <View style={styles.vSpacing8} />
            <Text style={[Font.subtitle, {color: colorStatus(item.status)}]}>
              {item.label_status}
            </Text>
          </View>
        </View>
        <View
          style={{
            borderBottomColor: Colors.container,
            borderBottomWidth: 1,
            marginTop: 8,
            marginBottom: 16,
          }}
        />
      </TouchableOpacity>
    );
  };

  const renderCancel = ({item}) => (
    <TouchableOpacity
      style={[{marginTop: 16}, item.desc ? false : {marginBottom: 8}]}>
      <View style={styles.wrapDetailOrder}>
        <View>
          <Text style={Font.subtitle}>{item.title}</Text>
          {item.desc === null ? (
            false
          ) : (
            <Text style={[Font.text, {color: Colors.placeholder}]}>
              {item.desc}
            </Text>
          )}
        </View>
        <MaterialCommunityIcons
          name="radiobox-blank"
          size={20}
          color={Colors.gray2}
        />
      </View>
      {item.desc === null ? false : <Horizontal />}
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <View
        style={{
          height: 56,
          elevation: 2,
          paddingHorizontal: 16,
          backgroundColor: Colors.white,
          justifyContent: 'center',
        }}>
        <TouchableOpacity style={styles.wrapBtnDate} onPress={() => onOpen()}>
          <AntDesign name="calendar" size={20} color={Colors.black} />
          <Text style={[Font.text, {marginHorizontal: 8}]}>Filter Tanggal</Text>
          <AntDesign name="down" size={16} color={Colors.placeholder} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={data}
        renderItem={renderListHistory}
        // horizontal
        style={{marginTop: 16}}
        ListHeaderComponent={<View style={{width: 16}} />}
        ListFooterComponent={<View style={{width: 8}} />}
        showsHorizontalScrollIndicator={false}
      />
      {/* modalFilter */}
      <Modalize ref={modalFilter} withHandle={false} modalHeight={520}>
        <View
          style={{
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}>
          <View style={{padding: 16}}>
            <View style={styles.wrapDetailOrder}>
              <View
                style={{flexDirection: 'row'}}
                onPress={() => onOpenFilter()}>
                <AntDesign
                  name="calendar"
                  size={20}
                  color={Colors.black}
                  style={{marginRight: 8}}
                />
                <Text style={Font.subtitle}>Filter Tanggal</Text>
              </View>
              <MaterialCommunityIcons
                name="close"
                size={24}
                color={Colors.black}
                onPress={() => onClose()}
              />
            </View>
            <View style={{height: 16}} />
            <Horizontal />
            <View style={{flex: 1}}>
              <FlatList
                // showsVerticalScrollIndicator={false}
                data={filterDate}
                renderItem={renderCancel}
                // ListEmptyComponent={
                //   <View
                //     style={{
                //       flex: 1,
                //       justifyContent: 'center',
                //       alignItems: 'center',
                //     }}>
                //     <Text style={styles.subtitle}>{'tunggu sebentar..'}</Text>
                //   </View>
                // }
              />
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <FormInput
                    lefticon={
                      <AntDesign
                        name="calendar"
                        size={20}
                        color={Colors.black}
                      />
                    }
                    titleCustom={<Text style={[Font.text]}>Tanggal Awal</Text>}
                    val={'02/12/2022'}
                    bottomZero={true}
                  />
                </View>
                <View style={{flex: 1}}>
                  <FormInput
                    lefticon={
                      <AntDesign
                        name="calendar"
                        size={20}
                        color={Colors.black}
                      />
                    }
                    titleCustom={<Text style={[Font.text]}>Tanggal Akhir</Text>}
                    val={'02/12/2022'}
                    bottomZero={true}
                  />
                </View>
              </View>
              <View style={{height: 16}} />
            </View>
          </View>
          <View style={styles.absoluteBottom}>
            <View style={{flex: 1, marginRight: 8}}>
              <BtnOutline title={'Hapus Filter'} color={Colors.error} />
            </View>
            <View style={{flex: 1}}>
              <BtnPrimary title={'terapkan'} pressed={() => onCloseFilter()} />
            </View>
          </View>
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default HistoryTU;

const styles = StyleSheet.create({
  vSpacing8: {height: 8},
  wrapBtnDate: {
    width: 153,
    flexDirection: 'row',
    borderWidth: 1,
    padding: 8,
    borderRadius: 8,
    borderColor: Colors.container,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapPlusIcon: {
    width: 40,
    height: 40,
    backgroundColor: Colors.primary1shading,
    padding: 8,
    borderRadius: 16,
  },
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 8,
  },
  absoluteBottom: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    height: 72,
    padding: 16,
    justifyContent: 'center',
    elevation: 10,
  },
});
