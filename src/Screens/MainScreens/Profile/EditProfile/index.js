import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../../Styles/Colors';
import FormInput from '../../../Compenent/FormInput';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import Font from '../../../../Styles/Fonts';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {key, getMe, postUpdateProfile} from '../../../../Variable/ApiClient';
import AlertMsg from '../../../Compenent/AlertMsg';
import ToastMsg from '../../../Compenent/ToastMsg';
import SpinnerLoad from '../../../Compenent/SpinnerLoad';
import {launchImageLibrary} from 'react-native-image-picker';

const EditProfile = ({navigation}) => {
  const [token, setToken] = useState('');
  const [form, setForm] = useState({
    photo: '',
    name: '',
    email: '',
    no_telp: '',
  });
  const [load, setLoad] = useState(false);
  const [isChose, setIsChose] = useState(false);
  const [image, setImage] = useState(null);

  const getToken = async () => {
    try {
      const auth = await AsyncStorage.getItem('api_token');
      return getData(auth);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const getData = auth => {
    setToken(auth);
    // get profile
    axios
      .get(getMe, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        res.data.status
          ? setForm({
              ...form,
              ['photo']: res.data.data.photo,
              ['name']: res.data.data.name,
              ['email']: res.data.data.email,
              ['no_telp']: res.data.data.no_telp,
            })
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 3000,
      maxHeight: 3000,
      quality: 1,
    };
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        ////console.log('User cancelled image picker');
      } else if (response.error) {
        ////console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // ////console.log(
        //   'User tapped custom button: ',
        //   response.customButton
        // );
        alert(response.customButton);
      } else {
        let tmpFiles = {
          uri: response.assets[0]?.uri,
          type: response.assets[0]?.type,
          name: response.assets[0]?.fileName,
        };
        setIsChose(true);
        setImage(response?.assets[0]?.uri);
        onInputChange(tmpFiles, 'photo');
      }

      // onInputChange(tmpFiles, 'file');
      // onUpdateProfile(tmpFiles)
    });
  };

  const submit = () => {
    const formsdata = new FormData();

    formsdata.append('name', form.name);
    formsdata.append('email', form.email);
    formsdata.append('no_telp', form.no_telp);
    isChose
      ? formsdata.append('photo', form.photo)
      : formsdata.append('photo', '');

    setLoad(true);
    axios
      .post(postUpdateProfile, formsdata, {
        headers: {
          'Content-Type': 'multipart/form-data',
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        setLoad(false);
        res.data.status
          ? ToastMsg('sukses merubah data')
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        setLoad(false);
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <View style={{height: 16}} />
      <SpinnerLoad loads={load} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center'}}>
          <View>
            <Image
              source={
                form.photo
                  ? isChose === false
                    ? {uri: form.photo}
                    : {uri: image}
                  : require('../../../../Assets/images/person1.png')
              }
              style={{width: 80, height: 80, borderRadius: 100}}
            />
            <TouchableOpacity
              style={styles.wrapBtnCamera}
              onPress={() => chooseFile('photo')}>
              <MaterialCommunityIcons
                name="camera"
                size={16}
                color={Colors.white}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{height: 13}} />
        <View style={{marginHorizontal: 16}}>
          <FormInput
            title={'Nama Lengkap'}
            val={form.name}
            change={value => onInputChange(value, 'name')}
          />
          <FormInput
            title={'No. Telpon'}
            val={form.no_telp}
            change={value => onInputChange(value, 'no_telp')}
          />
          <FormInput
            title={'Email'}
            val={form.email}
            change={value => onInputChange(value, 'email')}
          />
          <TouchableOpacity
            style={{alignItems: 'flex-end'}}
            onPress={() =>
              navigation.navigate('FormPassword', {type: 'change'})
            }>
            <Text style={[Font.bold, {color: Colors.primary1}]}>
              Ubah Password
            </Text>
          </TouchableOpacity>
          {/* <FormInput
            titleCustom={
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={[Font.bold, {marginLeft: 8}]}>Password</Text>
                <Text
                  style={[Font.bold, {color: Colors.primary1}]}
                  onPress={() =>
                    navigation.navigate('NewPassword', {type: 'change'})
                  }>
                  Ganti Password
                </Text>
              </View>
            }
            scureText={true}
          /> */}
        </View>
      </ScrollView>
      <View style={styles.absoluteBottom}>
        <BtnPrimary
          title={'Simpan Data'}
          pressed={() => submit()}
          //   pressed={() => navigation.navigate('CheckoutTU', {nominal: chosed})}
        />
      </View>
    </SafeAreaView>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  wrapBtnCamera: {
    padding: 4,
    backgroundColor: Colors.primary1,
    borderRadius: 100,
    position: 'absolute',
    bottom: 2,
    right: 2,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 72,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
});
