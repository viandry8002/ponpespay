import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {screenWidth, screenWidthMargin} from '../../../Styles/Styles';
import BtnOutline from '../../Compenent/BtnOutline';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FloatBalance from '../../Compenent/FloatBalance';
import Horizontal from '../../Compenent/Horizontal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {key, getMe, getWallet} from '../../../Variable/ApiClient';
import AlertMsg from '../../Compenent/AlertMsg';

const Profile = ({route, navigation}) => {
  const [profile, setProfile] = useState([]);
  const btnProfile = [
    {
      id: 1,
      title: 'Edit Profile',
      image: require('../../../Assets/icons/pencil.png'),
      nav: () => navigation.navigate('EditProfile'),
    },
    {
      id: 2,
      title: 'Data Alamat',
      image: require('../../../Assets/icons/pin.png'),
      nav: () =>
        navigation.navigate('FormAddress', {type: 'change', data: profile}),
    },
    {
      id: 3,
      title: 'Hubungi Kami',
      image: require('../../../Assets/icons/cs.png'),
      // nav: '',
    },
    {
      id: 4,
      title: 'Kebijakan Privasi',
      image: require('../../../Assets/icons/chield.png'),
      // nav: '',
    },
    {
      id: 5,
      title: 'Syarat Dan Ketentuan',
      image: require('../../../Assets/icons/dock.png'),
      // nav: '',
    },
  ];

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const getData = auth => {
    // get profile
    axios
      .get(getMe, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        res.data.status
          ? setProfile(res.data.data)
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <LinearGradient
          colors={[Colors.primary1, Colors.primary2]}
          start={{x: 0.0, y: 0.5}}
          end={{x: 1.0, y: 0.5}}
          style={{height: 112, width: screenWidth, position: 'absolute'}}
        />
        {/* absolute modal */}
        <View style={styles.wrapAbsoluteModal}>
          {/* profile */}
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Image
              source={{uri: profile.photo}}
              style={{width: 72, height: 72, borderRadius: 100}}
            />
            <View style={{flex: 1, marginLeft: 8, justifyContent: 'center'}}>
              <Text style={[Font.title]}>{profile.name}</Text>
              <Text style={[Font.text, {color: Colors.placeholder}]}>
                {profile.email}
              </Text>
            </View>
          </View>
          <View style={{height: 8}} />
          {/* balance */}
          <FloatBalance
            type={'Profile'}
            btnTopup={() => navigation.navigate('TopUp')}
            btnHistory={() => navigation.navigate('HistoryTU')}
            balance={profile.saldo}
          />
        </View>
        {/* Pembayaran */}
        <View style={{margin: 16}}>
          {/* Rincian Transaksi */}
          <View style={styles.spacing} />
          <View
            style={{
              borderWidth: 1,
              borderRadius: 8,
              borderColor: Colors.container,
              paddingHorizontal: 16,
              paddingBottom: 16,
            }}>
            {btnProfile.map((item, i) => (
              <TouchableOpacity key={i} onPress={item.nav}>
                <View style={styles.spacing} />
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    margin: 8,
                    // marginBottom: 12,
                    // backgroundColor:'red'
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={item.image}
                      style={{width: 24, height: 24}}
                    />
                    <Text style={[Font.subtitle2, {marginLeft: 8}]}>
                      {item.title}
                    </Text>
                  </View>
                  <AntDesign
                    name="right"
                    size={16}
                    color={Colors.placeholder}
                  />
                </View>
                <Horizontal />
              </TouchableOpacity>
            ))}
          </View>
          <View style={styles.spacing} />
          <View style={styles.spacing} />
          <BtnPrimary
            title={'Log Out'}
            pressed={() =>
              navigation.reset({
                index: 0,
                routes: [{name: 'Auth', params: {type: 'Login'}}],
              })
            }
            bgColor={Colors.error}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  wrapAbsoluteModal: {
    // marginTop: 39,
    marginHorizontal: 16,
    borderRadius: 8,
    // width: screenWidth - 32,
    padding: 8,
    backgroundColor: Colors.white,
    elevation: 10,
    shadowColor: 'grey',
  },

  spacing: {height: 16},
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  wrapCopy: {flexDirection: 'row', alignItems: 'center', flex: 1},
});
