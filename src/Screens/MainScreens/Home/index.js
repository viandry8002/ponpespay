import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
  FlatList,
} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import LinearGradient from 'react-native-linear-gradient';
import {TextMask} from 'react-native-masked-text';
import Octicons from 'react-native-vector-icons/Octicons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {screenWidthMargin, screenWidth} from '../../../Styles/Styles';
import WrapIcon from '../../Compenent/WrapIcon';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import TextShimmer from '../../Compenent/TextShimmer';
import FloatBalance from '../../Compenent/FloatBalance';
import axios from 'axios';
import {
  key,
  getHome,
  getWallet,
  getProducts,
} from '../../../Variable/ApiClient';
import AlertMsg from '../../Compenent/AlertMsg';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FormatMoney from '../../Compenent/FormatMoney';

const Icon1 = [
  {
    id: 1,
    title: 'Pulsa',
    image: require('../../../Assets/icons/pulsa.png'),
    nav: 'PulsaPaket',
  },
  {
    id: 2,
    title: 'Paket Data',
    image: require('../../../Assets/icons/paket.png'),
    nav: 'PulsaPaket',
  },
  {
    id: 3,
    title: 'Voucher Game',
    image: require('../../../Assets/icons/vgame.png'),
    nav: 'VoucherGame',
  },
  {
    title: '',
    image: '',
  },
];

const Icon2 = [
  {
    title: 'Telkom',
    image: require('../../../Assets/icons/telkom.png'),
    nav: 'Telkom',
  },
  {
    title: 'Gas Alam',
    image: require('../../../Assets/icons/gas.png'),
    nav: 'GasAlam',
  },
  {
    title: 'BPJS',
    image: require('../../../Assets/icons/bpjs.png'),
    nav: 'Bpjs',
  },
  {
    title: 'Angsuran Kredit',
    image: require('../../../Assets/icons/kredit.png'),
    nav: 'AngsuranKredit',
  },
];

const Icon3 = [
  {
    title: 'Tagihan Listrik',
    image: require('../../../Assets/icons/listrik.png'),
    nav: 'TagihanListrik',
  },
  {
    title: 'PDAM',
    image: require('../../../Assets/icons/pdam.png'),
    nav: 'Pdam',
  },
  {
    title: 'Internet & TV Kabel',
    image: require('../../../Assets/icons/tv.png'),
    nav: 'InternetTv',
  },
  {
    title: 'Pascabayar',
    image: require('../../../Assets/icons/pascabayar.png'),
    nav: 'Pascabayar',
  },
];

// const Product = [
//   {
//     title: 'Teh Pucuk Harum 250ml',
//     image: require('../../../Assets/images/product/pucukHarum.png'),
//     price: 'Rp100.000',
//     discFrom: 'Rp200.000',
//     stock: '32',
//   },
//   {
//     title: 'Indomie Mie Goreng Original 85gr',
//     image: require('../../../Assets/images/product/indomie.png'),
//     price: 'Rp100.000',
//     discFrom: null,
//     stock: '32',
//   },
//   {
//     title: 'Pocari Sweat 500ml',
//     image: require('../../../Assets/images/product/pucukHarum.png'),
//     price: 'Rp100.000',
//     discFrom: 'Rp200.000',
//     stock: '32',
//   },
//   {
//     title: 'Tolak Angin Sidomuncul 15ml',
//     image: require('../../../Assets/images/product/indomie.png'),
//     price: 'Rp100.000',
//     discFrom: null,
//     stock: '32',
//   },
//   {
//     title: 'Krisbow Obeng',
//     image: require('../../../Assets/images/product/pucukHarum.png'),
//     price: 'Rp100.000',
//     discFrom: 'Rp200.000',
//     stock: '32',
//   },
// ];

const Home = ({navigation}) => {
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [slider, setSlider] = useState([]);
  const [wallet, setWallet] = useState([]);
  const [product, setProduct] = useState([]);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      // setLoading(false);
      getToken();
    }, 1000);
  }, []);

  const getData = auth => {
    setLoading(false);
    // get slider
    axios
      .get(getHome, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        res.data.status
          ? (setSlider(res.data.data.slider),
            setWallet(res.data.data.saldo),
            setProduct(res.data.data.product_recommended))
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const onRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      setRefresh(false);
    }, 1000);
  };

  // const images = [
  //   require('../../../Assets/images/banner1.png'),
  //   require('../../../Assets/images/banner1.png'),
  //   require('../../../Assets/images/banner1.png'),
  // ];

  const images = slider.map((datas, index) => {
    return [{uri: datas.file}];
  });

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refresh}
            onRefresh={onRefresh}
            colors={[Colors.primary1]}
          />
        }>
        <LinearGradient
          colors={[Colors.primary1, Colors.primary2]}
          start={{x: 0.0, y: 0.5}}
          end={{x: 1.0, y: 0.5}}
          style={{height: 56, position: 'absolute', width: screenWidth}}
        />

        {loading === true ? (
          <View style={{flex: 1}}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                height: 165,
                width: screenWidthMargin,
                borderRadius: 10,
                alignSelf: 'center',
              }}
            />
            <View style={{height: 26}} />
            {/* float saldo */}
            <View style={[styles.shadow, styles.wrapFloatSaldo]}>
              <View style={{flex: 1}}>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: 8,
                    alignItems: 'center',
                  }}>
                  <TextShimmer type={'normal'} />
                </View>
                <TextShimmer type={'large'} />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  flex: 1,
                }}>
                <TouchableOpacity style={styles.btnFloatSaldo}>
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    style={styles.wrapBtnSaldo}
                  />
                  <TextShimmer type={'normal'} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnFloatSaldo}>
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    style={styles.wrapBtnSaldo}
                  />
                  <TextShimmer type={'normal'} />
                </TouchableOpacity>
              </View>
            </View>

            <View style={{height: 16}} />
            <View style={{marginHorizontal: 16}}>
              <TextShimmer type={'large'} />
              <View style={styles.rowMargin}>
                {Icon1.map((data, i) =>
                  data.title === '' && data.image === '' ? (
                    <View style={{flex: 1}} key={i} />
                  ) : (
                    <TouchableOpacity style={styles.centerFlex} key={i}>
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        style={[styles.wrapIcon, styles.shadow]}
                      />
                      <TextShimmer type={'normal'} />
                    </TouchableOpacity>
                  ),
                )}
              </View>

              <View style={{marginVertical: 16}}>
                <TextShimmer type={'large'} />
                <View style={styles.rowMargin}>
                  {Icon2.map((data, i) =>
                    data.title === '' && data.image === '' ? (
                      <View style={{flex: 1}} key={i} />
                    ) : (
                      <TouchableOpacity style={styles.centerFlex} key={i}>
                        <ShimmerPlaceHolder
                          LinearGradient={LinearGradient}
                          style={[styles.wrapIcon, styles.shadow]}
                        />
                        <TextShimmer type={'normal'} />
                      </TouchableOpacity>
                    ),
                  )}
                </View>
                <View style={styles.rowMargin}>
                  {Icon3.map((data, i) =>
                    data.title === '' && data.image === '' ? (
                      <View style={{flex: 1}} key={i} />
                    ) : (
                      <TouchableOpacity style={styles.centerFlex} key={i}>
                        <ShimmerPlaceHolder
                          LinearGradient={LinearGradient}
                          style={[styles.wrapIcon, styles.shadow]}
                        />
                        <TextShimmer type={'normal'} />
                      </TouchableOpacity>
                    ),
                  )}
                </View>
              </View>
              <View style={{height: 16}} />
              <TextShimmer type={'large'} />
            </View>
            <FlatList
              data={product}
              renderItem={({item}) => (
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={{
                    width: 130,
                    height: 227,
                    marginRight: 8,
                    borderRadius: 10,
                  }}
                />
              )}
              horizontal
              style={{marginTop: 16}}
              ListHeaderComponent={<View style={{width: 16}} />}
              ListFooterComponent={<View style={{width: 8}} />}
              showsHorizontalScrollIndicator={false}
            />
            <View style={{height: 40}} />
          </View>
        ) : (
          <View style={{flex: 1}}>
            <View style={{alignItems: 'center'}}>
              <View style={{paddingHorizontal: 16, height: 190}}>
                <SliderBox
                  paginationBoxVerticalPadding={10}
                  autoplay
                  circleLoop
                  images={images}
                  dotColor={Colors.primary1}
                  inactiveDotColor={Colors.gray5}
                  sliderBoxHeight={180}
                  parentWidth={screenWidthMargin}
                  ImageComponentStyle={{borderRadius: 10, height: 165}}
                  dotStyle={{
                    width: 8,
                    height: 8,
                    marginHorizontal: -10,
                    left: -20,
                  }}
                  paginationBoxStyle={{
                    alignSelf: 'flex-start',
                  }}
                  imageLoadingColor={Colors.white}
                />
              </View>
            </View>
            <View style={{height: 10}} />
            {/* float saldo */}
            {/* balance */}
            <FloatBalance
              type={'Home'}
              btnTopup={() => navigation.navigate('TopUp')}
              btnHistory={() => navigation.navigate('HistoryTU')}
              balance={wallet}
            />
            <View style={{height: 16}} />
            <View style={{marginHorizontal: 16}}>
              <Text style={Font.subtitle}>Top Up & Pembelian</Text>
              <View style={styles.rowMargin}>
                {Icon1.map((data, i) =>
                  data.title === '' && data.image === '' ? (
                    <View style={{flex: 1}} key={i} />
                  ) : (
                    <TouchableOpacity
                      key={i}
                      style={styles.centerFlex}
                      onPress={() => navigation.navigate(data.nav, data)}>
                      <WrapIcon icon={data.image} />
                      <Text style={Font.caption}>{data.title}</Text>
                    </TouchableOpacity>
                  ),
                )}
              </View>

              <View style={{marginVertical: 16}}>
                <Text style={Font.subtitle}>Tagihan & Pembayaran</Text>
                <View style={styles.rowMargin}>
                  {Icon2.map((data, i) =>
                    data.title === '' && data.image === '' ? (
                      <View style={{flex: 1}} key={i} />
                    ) : (
                      <TouchableOpacity
                        key={i}
                        style={styles.centerFlex}
                        onPress={() => navigation.navigate(data.nav, data)}>
                        <WrapIcon icon={data.image} />
                        <Text style={Font.caption}>{data.title}</Text>
                      </TouchableOpacity>
                    ),
                  )}
                </View>
                <View style={styles.rowMargin}>
                  {Icon3.map((data, i) =>
                    data.title === '' && data.image === '' ? (
                      <View style={{flex: 1}} key={i} />
                    ) : (
                      <TouchableOpacity
                        key={i}
                        style={styles.centerFlex}
                        onPress={() => navigation.navigate(data.nav, data)}>
                        <WrapIcon icon={data.image} />
                        <Text style={Font.caption}>{data.title}</Text>
                      </TouchableOpacity>
                    ),
                  )}
                </View>
              </View>

              <View style={{height: 16}} />
              <Text style={Font.subtitle}>Produk Unggulan</Text>
            </View>
            <FlatList
              data={product}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{
                    width: 130,
                    height: 227,
                    marginRight: 8,
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: Colors.container,
                  }}>
                  <Image
                    source={{uri: item.file}}
                    style={{
                      width: '100%',
                      height: 130,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                    }}
                  />
                  <View style={{flex: 1, margin: 8}}>
                    <Text style={Font.text}>{item.title}</Text>
                    <FormatMoney value={item.price} style={Font.bold} />
                    {item.discFrom != null ? (
                      <Text
                        style={[
                          Font.caption,
                          {
                            color: Colors.placeholder,
                            textDecorationLine: 'line-through',
                          },
                        ]}>
                        {item.discFrom}
                      </Text>
                    ) : (
                      false
                    )}
                    <Text style={Font.text}>Stok : {item.stock}</Text>
                  </View>
                </TouchableOpacity>
              )}
              horizontal
              style={{marginTop: 16}}
              ListHeaderComponent={<View style={{width: 16}} />}
              ListFooterComponent={<View style={{width: 8}} />}
              showsHorizontalScrollIndicator={false}
            />
            <View style={{height: 40}} />
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  wrapFloatSaldo: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    height: 94,
    alignSelf: 'center',
    paddingHorizontal: 16,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnFloatSaldo: {alignItems: 'center', justifyContent: 'space-around'},
  wrapBtnSaldo: {
    width: 42,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.container,
    borderRadius: 15,
    borderWidth: 1,
    marginBottom: 8,
  },
  shadow: {
    shadowColor: 'grey',
    elevation: 15,
  },
  centerFlex: {alignItems: 'center', flex: 1},
  wrapIcon: {
    width: 46,
    height: 46,
    backgroundColor: Colors.white,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 8,
  },
  rowMargin: {marginTop: 16, flexDirection: 'row'},
});
