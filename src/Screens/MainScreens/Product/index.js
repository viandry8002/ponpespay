import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Input} from '@rneui/themed';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {
  key,
  getProductsCategories,
  getProducts,
} from '../../../Variable/ApiClient';
import ListProduct from '../../MultipleScreens/ListProduct';
import MasonryList from '@react-native-seoul/masonry-list';
import FormatMoney from '../../Compenent/FormatMoney';

const Tab = createMaterialTopTabNavigator();

// const topTab = [
//   {
//     id: 1,
//     title: 'Semua',
//     name: 'Semua',
//     comp: ListProduct,
//   },
//   {
//     id: 2,
//     title: 'Makanan',
//     name: 'Makanan',
//     comp: ListProduct,
//   },
//   {
//     id: 3,
//     title: 'Minuman',
//     name: 'Minuman',
//     comp: ListProduct,
//   },
//   {
//     id: 4,
//     title: 'Obat Obatan',
//     name: 'Obat Obatan',
//     comp: ListProduct,
//   },
//   {
//     id: 5,
//     title: 'Elektronik',
//     name: 'Elektroni',
//     comp: ListProduct,
//   },
// ];

const Blank = () => <View />;

const Product = () => {
  const [categories, setCategories] = useState([
    {
      id: 1,
      title: 'All',
      comp: Blank,
    },
  ]);
  const [search, setSearch] = useState('');
  console.log('ppp', search);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get(getProductsCategories, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        const aa = res.data.data;

        aa.map((dat, i) => (aa[i] = {...aa[i], comp: ListProd}));

        // categories.push(aa);
        // console.log('kkk',aa);
        setCategories(aa);
        // categories.push(aa);

        // categories.push({
        //   id: 0,
        //   title: 'Semua',
        //   comp: ListProduct,
        // });
      })
      .catch(err => {
        console.log(err);
      });
  };

  const ListProd = ({route}) => {
    const [dataProd, setdataProd] = useState([]);
    const [refreshProduct, setrefreshProduct] = useState(false);

    useEffect(() => {
      getListProduct();
    }, []);

    const getListProduct = () => {
      setrefreshProduct(true);
      axios
        .get(
          getProducts +
            '?category_id=' +
            (route.params.id === 5 ? 0 : route.params.id) +
            '&q=' +
            search,
        )
        .then(res => {
          setrefreshProduct(false);
          setdataProd(res.data.status ? res.data.data : []);
        })
        .catch(err => {
          setrefreshProduct(true);
          console.log(err);
        });
    };

    return (
      <ListProduct
        dataList={dataProd}
        onRefresh={getListProduct}
        refresh={refreshProduct}
      />
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      /> */}
      {/* <View style={{height: 52}} /> */}
      <LinearGradient
        start={{x: 0.0, y: 0.5}}
        end={{x: 1.0, y: 0.5}}
        colors={[Colors.primary1, Colors.primary2]}
        style={{
          paddingHorizontal: 16,
          height: 108,
          paddingTop: 52,
        }}>
        <Input
          label={false}
          placeholder={'Cari Sesuatu'}
          style={[Font.subtitle2, {color: Colors.placholder}]}
          inputContainerStyle={{
            marginHorizontal: 0,
            borderBottomWidth: 0,
            backgroundColor: Colors.white,
            borderRadius: 8,
            paddingHorizontal: 8,
          }}
          value={search}
          onChangeText={value => setSearch(value)}
          onSubmitEditing={() => getData()}
          containerStyle={{paddingHorizontal: 0}}
          inputStyle={{paddingHorizontal: 0, marginLeft: 16}}
          leftIcon={() => (
            <AntDesign name="search1" size={20} color={Colors.placeholder} />
          )}
        />
      </LinearGradient>

      <Tab.Navigator
        // tabBar={props => console.log('sssssssssssdd', props.position)}
        // tabBarPosition="bottom"
        // accessibilityState={isFocused => console.log('ggggg', isFocused)}

        screenOptions={({route}) =>
          // console.log('ggggg', route.name),
          ({
            tabBarLabelStyle: {
              fontWeight: '400',
              fontSize: 12,
              textTransform: 'none',
              bottom: 6,
            },
            tabBarItemStyle: {width: 100},
            tabBarStyle: {
              backgroundColor: Colors.white,
              height: 38,
              justifyContent: 'center',
            },
            tabBarActiveTintColor: Colors.primary1,
            tabBarInactiveTintColor: Colors.gray3,
            tabBarIndicatorStyle: {backgroundColor: Colors.primary1},
            tabBarScrollEnabled: true,
          })
        }>
        {
          // categories.length > 0
          //   ?
          categories.map((data, i) => (
            <Tab.Screen
              key={i}
              name={data.title}
              component={data.comp}
              options={{title: data.title}}
              initialParams={{
                id: data.id,
                title: data.title,
              }}
            />
          ))
          // : false
        }
        {/* <Tab.Screen
          name="Feed"
          component={FeedScreen}
          options={{tabBarLabel: 'Home'}}
        />
        <Tab.Screen
          name="Notifications"
          component={NotificationsScreen}
          options={{tabBarLabel: 'Updates'}}
        />
        <Tab.Screen
          name="Profile"
          component={ProfileScreen}
          options={{tabBarLabel: 'Profile'}}
        /> */}
      </Tab.Navigator>
    </SafeAreaView>
  );
};

export default Product;

const styles = StyleSheet.create({});
