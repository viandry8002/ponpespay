import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Input} from '@rneui/themed';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {key, getProductsCategories} from '../../../Variable/ApiClient';
import ListProduct from '../../MultipleScreens/ListProduct';
import MasonryList from '@react-native-seoul/masonry-list';

const Tab = createMaterialTopTabNavigator();

const topTab = [
  {
    id: 1,
    title: 'Semua',
    name: 'Semua',
    component: ListProduct,
  },
  {
    id: 2,
    title: 'Makanan',
    name: 'Makanan',
    component: ListProduct,
  },
  {
    id: 3,
    title: 'Minuman',
    name: 'Minuman',
    component: ListProduct,
  },
  {
    id: 4,
    title: 'Obat Obatan',
    name: 'Obat Obatan',
    component: ListProduct,
  },
  {
    id: 5,
    title: 'Elektronik',
    name: 'Elektroni',
    component: ListProduct,
  },
];

const Product = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get(getProductsCategories, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        // console.log('haha', res.data.data);
        const aa = res.data.data;
        // console.log('gaga',aa);
        aa.map(
          (dat, i) =>
            // arr.splice(i, 0, {component: 'haha'});
            // dat[i].append({component: 'haha'}),
            // console.log('gagaga', dat),
            //   ...dat,
            //   ['aa']: 'aaa'
            (aa[i] = {...aa[i], component: <ListProduct />}),
        );

        // aa.splice(1, 0, {component: 'haha'});

        // console.log('aag', aa);
        setCategories(aa);

        // res.data.success
        //   ? setCategories(res.data.data[0])
        //   : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      /> */}
      {/* <View style={{height: 52}} /> */}
      <LinearGradient
        start={{x: 0.0, y: 0.5}}
        end={{x: 1.0, y: 0.5}}
        colors={[Colors.primary1, Colors.primary2]}
        style={{
          paddingHorizontal: 16,
          height: 108,
          paddingTop: 52,
        }}>
        <Input
          label={false}
          placeholder={'Cari Sesuatu'}
          style={[Font.subtitle2, {color: Colors.placholder}]}
          inputContainerStyle={{
            marginHorizontal: 0,
            borderBottomWidth: 0,
            backgroundColor: Colors.white,
            borderRadius: 8,
            paddingHorizontal: 8,
          }}
          containerStyle={{paddingHorizontal: 0}}
          inputStyle={{paddingHorizontal: 0, marginLeft: 16}}
          leftIcon={() => (
            <AntDesign name="search1" size={20} color={Colors.placeholder} />
          )}
        />
      </LinearGradient>

      <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: {
            fontWeight: '400',
            fontSize: 12,
            textTransform: 'none',
            bottom: 6,
          },
          tabBarItemStyle: {width: 100},
          tabBarStyle: {
            backgroundColor: Colors.white,
            height: 38,
            justifyContent: 'center',
          },
          tabBarActiveTintColor: Colors.primary1,
          tabBarInactiveTintColor: Colors.gray3,
          tabBarIndicatorStyle: {backgroundColor: Colors.primary1},
          tabBarScrollEnabled: true,
        }}>
        {categories.length > 0
          ? categories.map((data, i) => {
              <Tab.Screen
                key={i}
                name={data.title}
                component={() => data.component}
                options={{title: data.title}}
              />;
            })
          : false}
      </Tab.Navigator>
    </SafeAreaView>
  );
};

export default Product;

const styles = StyleSheet.create({});
