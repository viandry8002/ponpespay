import React, {useEffect, useState, useRef} from 'react';
import {
  FlatList,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import {Input} from 'react-native-elements';
import {TextMask} from 'react-native-masked-text';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Horizontal from '../../Compenent/Horizontal';
import WrapIcon from '../../Compenent/WrapIcon';
import {Modalize} from 'react-native-modalize';
import BtnPrimary from '../../Compenent/BtnPrimary';
import BtnOutline from '../../Compenent/BtnOutline';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FormInput from '../../Compenent/FormInput';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {key, getTransactionHistory} from '../../../Variable/ApiClient';
import moment from 'moment';
import FormatMoney from '../../Compenent/FormatMoney';

const history = [
  {
    date: '12 Desember 2021 ',
    data: [
      {
        title: 'Belanja Produk',
        no: '10 Item',
        status: 'Menunggu Pembayaran',
        harga: '120.000',
        type: 'produk',
      },
      {
        title: 'Belanja Produk',
        no: '10 Item',
        status: 'Diproses',
        harga: '120.000',
        type: 'produk',
      },
      {
        title: 'Belanja Produk',
        no: '10 Item',
        status: 'Selesai',
        harga: '120.000',
        type: 'produk',
      },
      {
        title: 'Belanja Produk',
        no: '10 Item',
        status: 'Gagal',
        harga: '120.000',
        type: 'produk',
      },
      {
        title: 'Telkomsel - Simpati 10.000',
        no: '089238122131',
        status: 'Diproses',
        harga: '120.000',
        type: 'pulsa',
      },
      {
        title: 'Token Listrik Rp50.000',
        no: '089238122131',
        status: 'Selesai',
        harga: '120.000',
        type: 'listrik',
      },
      {
        title: 'Indosat - IM3 5.000',
        no: '089238122131',
        status: 'Gagal',
        harga: '120.000',
        type: 'paket',
      },
    ],
  },
  {
    date: '11 Desember 2021 ',
    data: [
      {
        title: 'PDAM KAB. BOGOR',
        no: '089238122131',
        status: 'Selesai',
        harga: '120.000',
        type: 'pdam',
      },
      {
        title: 'Tri AON 2GB',
        no: '089238122131',
        status: 'Gagal',
        harga: '120.000',
        type: 'pulsa',
      },
    ],
  },
];

const Icon1 = [
  {
    id: 1,
    title: 'Produk',
    image: require('../../../Assets/icons/shopping.png'),
    nav: 'Produk',
  },
  {
    id: 2,
    title: 'Pulsa',
    image: require('../../../Assets/icons/pulsa.png'),
    nav: 'PulsaPaket',
  },
  {
    id: 3,
    title: 'Paket Data',
    image: require('../../../Assets/icons/paket.png'),
    nav: 'PulsaPaket',
  },
  {
    id: 4,
    title: 'Voucher Game',
    image: require('../../../Assets/icons/vgame.png'),
    nav: 'VoucherGame',
  },
  {
    id: 5,
    title: 'Telkom',
    image: require('../../../Assets/icons/telkom.png'),
    nav: 'Telkom',
  },
  {
    id: 6,
    title: 'Gas Alam',
    image: require('../../../Assets/icons/gas.png'),
    nav: 'GasAlam',
  },
  {
    id: 7,
    title: 'BPJS',
    image: require('../../../Assets/icons/bpjs.png'),
    nav: 'Bpjs',
  },
  {
    id: 8,
    title: 'Angsuran Kredit',
    image: require('../../../Assets/icons/kredit.png'),
    nav: 'AngsuranKredit',
  },
  {
    id: 9,
    title: 'Tagihan Listrik',
    image: require('../../../Assets/icons/listrik.png'),
    nav: 'TagihanListrik',
  },
  {
    id: 10,
    title: 'PDAM',
    image: require('../../../Assets/icons/pdam.png'),
    nav: 'Pdam',
  },
  {
    id: 11,
    title: 'Internet & TV Kabel',
    image: require('../../../Assets/icons/tv.png'),
    nav: 'InternetTv',
  },
  {
    id: 12,
    title: 'Pascabayar',
    image: require('../../../Assets/icons/pascabayar.png'),
    nav: 'Pascabayar',
  },
];

const Transaction = ({navigation}) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState(1);

  const modalFilter = useRef(null);

  const onOpenFilter = () => {
    modalFilter.current?.open();
    // console.log('aaa', modalFilter.current.open);
  };

  const onCloseFilter = modal => {
    modalFilter.current?.close();
  };

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      // setData(history);
      getToken();
      setLoading(false);
    }, 1000);
  }, []);

  const getData = auth => {
    axios
      .get(getTransactionHistory, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        res.data.success
          ? setData(res.data.data.data)
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const onRefresh = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  const textStatus = status => {
    switch (status) {
      case 'Selesai':
        return Colors.success;
      case 'Diproses':
        return Colors.primary1;
      case 'unpaid':
        return 'Menunggu Pembayaran';
      default:
        return Colors.error;
    }
  };

  const colorStatus = status => {
    switch (status) {
      case 'Selesai':
        return Colors.success;
      case 'Diproses':
        return Colors.primary1;
      case 'unpaid':
        return Colors.yellow;
      default:
        return Colors.error;
    }
  };

  const typeIcon = type => {
    switch (type) {
      case 'pulsa':
        return require('../../../Assets/icons/pulsa.png');
      case 'paket':
        return require('../../../Assets/icons/paket.png');
      case 'game':
        return require('../../../Assets/icons/vgame.png');

      case 'telkom':
        return require('../../../Assets/icons/telkom.png');
      case 'gas':
        return require('../../../Assets/icons/gas.png');
      case 'bpjs':
        return require('../../../Assets/icons/bpjs.png');
      case 'kredit':
        return require('../../../Assets/icons/kredit.png');

      case 'listrik':
        return require('../../../Assets/icons/listrik.png');
      case 'pdam':
        return require('../../../Assets/icons/pdam.png');
      case 'tv':
        return require('../../../Assets/icons/tv.png');
      case 'pascabayar':
        return require('../../../Assets/icons/pascabayar.png');

      case 'product':
        return require('../../../Assets/icons/shopping.png');

      default:
        return require('../../../Assets/icons/pulsa.png');
    }
  };

  const renderHistory = (item, i) => (
    <TouchableOpacity
      key={i}
      style={{backgroundColor: Colors.white, height: 74}}
      onPress={() =>
        item.key_source === 'product'
          ? navigation.navigate('DtlTransaksiProduct', {id: item.id})
          : navigation.navigate('DtlTransaksi', item.status)
      }>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          marginHorizontal: 16,
        }}>
        <WrapIcon icon={typeIcon(item.key_source)} />
        <View style={{flex: 3, marginLeft: 8}}>
          <Text style={Font.bold}>{item.title}</Text>
          <Text style={[Font.caption, {color: Colors.placeholder}]}>
            {item.item}
          </Text>
        </View>
        <View style={{flex: 1}}>
          <Text
            style={[
              Font.captionBold,
              {textAlign: 'right', color: colorStatus(item.status)},
            ]}>
            {textStatus(item.status)}
          </Text>
          <FormatMoney
            value={item.total}
            style={[Font.text, {textAlign: 'right'}]}
          />
        </View>
      </View>
      <Horizontal />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.wrapHeader}>
        <Input
          label={false}
          placeholder={'Cari History'}
          leftIcon={
            <View style={{padding: 8}}>
              <AntDesign name="search1" size={18} color={Colors.primary1} />
            </View>
          }
          style={[Font.text, {color: Colors.placeholder}]}
          inputContainerStyle={styles.inputSearch}
          containerStyle={{
            paddingHorizontal: 0,
            flex: 1,
            marginTop: 8,
          }}
          // containerStyle={{flex: 1, paddingTop: 8}}
          // value={form.email}
          // onChangeText={value => onInputChange(value, 'email')}
          errorStyle={{height: 0}}
        />
        <TouchableOpacity
          style={[styles.btnFilter, {backgroundColor: Colors.primary1shading}]}
          onPress={() => onOpenFilter()}>
          <AntDesign
            name="filter"
            size={18}
            style={{marginRight: 8}}
            color={Colors.primary1}
          />
          <Text style={[Font.text, {color: Colors.primary1}]}>
            Filter Aktif
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={styles.btnFilter}
          onPress={() => onOpenFilter()}>
          <AntDesign name="filter" size={18} style={{marginRight: 8}} />
          <Text style={Font.text}>Filter</Text>
        </TouchableOpacity> */}
      </View>
      <View
        style={{
          padding: 16,
          backgroundColor: Colors.white,
          height: 72,
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            padding: 8,
            flexDirection: 'row',
            elevation: 2,
            borderRadius: 8,
            alignItems: 'center',
          }}>
          <Ionicons name="time-outline" size={24} color={Colors.primary1} />
          <Text style={[Font.subtitle2, {marginHorizontal: 8, flex: 1}]}>
            Menunggu Pembayaran
          </Text>
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 100,
              backgroundColor: Colors.error,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={[Font.subtitle2, {color: Colors.white}]}>1</Text>
          </View>
        </View>
      </View>

      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={onRefresh}
            colors={[Colors.primary1]}
          />
        }>
        {data?.map((d, i) => (
          <View key={i}>
            <View
              style={{
                height: 29,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={[Font.text, {color: Colors.placeholder}]}>
                {moment(d.date).format('DD MMMM YYYY')}
              </Text>
            </View>
            {d?.collection.map((dt, i) => renderHistory(dt, i))}
            {/* <FlatList
              data={d.data}
              renderItem={renderHistory}
              // ListHeaderComponent={(item) => (<View><Text>{item.date}</Text></View>)}
            /> */}
          </View>
        ))}
      </ScrollView>
      {/* modalize Product */}
      <Modalize ref={modalFilter} withHandle={false} modalHeight={600}>
        <View
          style={{
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}>
          <View style={{padding: 16}}>
            <View style={styles.wrapDetailOrder}>
              <View
                style={{flexDirection: 'row'}}
                onPress={() => onOpenFilter()}>
                <AntDesign name="filter" size={18} style={{marginRight: 8}} />
                <Text style={Font.subtitle}>Filter</Text>
              </View>
              <MaterialCommunityIcons
                name="close"
                size={24}
                color={Colors.black}
                onPress={() => onCloseFilter()}
              />
            </View>
            <View style={{height: 16}} />
            <Horizontal />
            <View style={{height: 16}} />
            <View style={{flex: 1}}>
              <Text style={Font.subtitle}>Filter Waktu</Text>
              <View style={{height: 16}} />
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <FormInput
                    lefticon={
                      <AntDesign
                        name="calendar"
                        size={20}
                        color={Colors.black}
                      />
                    }
                    titleCustom={<Text style={[Font.text]}>Tanggal Awal</Text>}
                    val={'02/12/2022'}
                    bottomZero={true}
                  />
                </View>
                <View style={{flex: 1}}>
                  <FormInput
                    lefticon={
                      <AntDesign
                        name="calendar"
                        size={20}
                        color={Colors.black}
                      />
                    }
                    titleCustom={<Text style={[Font.text]}>Tanggal Akhir</Text>}
                    val={'02/12/2022'}
                    bottomZero={true}
                  />
                </View>
              </View>
              <View style={{height: 16}} />
              <Text style={Font.subtitle}>Filter Layanan</Text>
              <View style={{height: 16}} />
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {Icon1.map((data, i) =>
                  data.title === '' && data.image === '' ? (
                    <View style={{flex: 1}} key={i} />
                  ) : (
                    <TouchableOpacity
                      style={{
                        width: 70,
                        height: 80,
                        borderColor:
                          data.id === selected
                            ? Colors.primary1
                            : Colors.container,
                        borderWidth: 1,
                        margin: 8,
                        borderRadius: 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor:
                          data.id === selected
                            ? Colors.primary1shading
                            : Colors.white,
                      }}
                      onPress={() => setSelected(data.id)}
                      key={i}>
                      <Image
                        source={data.image}
                        style={{width: 32, height: 32}}
                      />
                      <View style={{height: 8}} />
                      <Text style={[Font.caption]}>{data.title}</Text>
                    </TouchableOpacity>
                  ),
                )}
              </View>
            </View>
          </View>
          <View style={styles.absoluteBottom}>
            <View style={{flex: 1, marginRight: 8}}>
              <BtnOutline title={'Hapus Filter'} color={Colors.error} />
            </View>
            <View style={{flex: 1}}>
              <BtnPrimary title={'terapkan'} pressed={() => onCloseFilter()} />
            </View>
          </View>
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default Transaction;

const styles = StyleSheet.create({
  wrapHeader: {
    // flex: 1,
    // height: 50,
    backgroundColor: Colors.white,
    paddingHorizontal: 16,
    // paddingVertical: 8,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    elevation: 2,
  },
  inputSearch: {
    borderWidth: 1,
    height: 40,
    borderColor: Colors.container,
    borderRadius: 10,
    // width: 253,
  },
  btnFilter: {
    // flex: 1,
    // width: 67,
    height: 40,
    borderWidth: 1,
    borderColor: Colors.container,
    borderRadius: 10,
    padding: 8,
    marginLeft: 8,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  absoluteBottom: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    height: 72,
    padding: 16,
    justifyContent: 'center',
    elevation: 10,
  },
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  centerFlex: {alignItems: 'center', flex: 1},
});
