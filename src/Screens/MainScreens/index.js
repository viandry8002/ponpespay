import SplashScreen from './SplashScreen';
import Onboarding from './Onboarding';
import Auth from '../MultipleScreens/Auth';
import EmailForgot from './Auth/EmailForgot';
import OtpVerif from '../MultipleScreens/OtpVerif';
import FormAddress from '../MultipleScreens/FormAddress';
import FormPassword from '../MultipleScreens/FormPassword';
import Success from '../MultipleScreens/Success';

import Home from './Home';
// top up
import TopUp from './TopUp';
import CheckoutTU from './TopUp/CheckoutTU';
import SuccessTU from './TopUp/SuccessTU';
import HistoryTU from './TopUp/HistoryTU';
import DtlTransactionTU from './TopUp/DtlTransactionTU';

import Transaction from './Transaction';
import DtlTransaksi from '../MultipleScreens/DtlTransaksi';
import DtlTransaksiProduct from '../MultipleScreens/DtlTransaksiProduct';
import Product from './Product';
import Checkout from './Checkout';
import PaymentMethod from './Checkout/PaymentMethod';
import SuccessCheckoutProduct from './Checkout/SuccessCheckoutProduct';

import Profile from './Profile';
import EditProfile from './Profile/EditProfile';
// Top Up & Pembelian
import PulsaPaket from './ServicesTopUp/PulsaPaket';
import VoucherGame from './ServicesTopUp/VoucherGame';
import JenisVoucherGame from './ServicesTopUp/VoucherGame/JenisVoucherGame';
// Tagihan & Pembayaran
import Telkom from './ServicesBill/Telkom';
import GasAlam from './ServicesBill/GasAlam';
import Bpjs from './ServicesBill/Bpjs';
import AngsuranKredit from './ServicesBill/AngsuranKredit';
import TagihanListrik from './ServicesBill/TagihanListrik';
import Pdam from './ServicesBill/Pdam';
import InternetTv from './ServicesBill/InternetTv';
import Pascabayar from './ServicesBill/Pascabayar';

import DtlTransactionService from '../MultipleScreens/DtlTransactionService';
import SuccessTransactionService from '../MultipleScreens/SuccessTransactionService';

export {
  Onboarding,
  SplashScreen,
  Auth,
  EmailForgot,
  OtpVerif,
  FormAddress,
  FormPassword,
  Success,
  Home,
  TopUp,
  CheckoutTU,
  SuccessTU,
  HistoryTU,
  DtlTransactionTU,
  Transaction,
  DtlTransaksi,
  DtlTransaksiProduct,
  Product,
  Checkout,
  PaymentMethod,
  SuccessCheckoutProduct,
  Profile,
  EditProfile,
  PulsaPaket,
  VoucherGame,
  JenisVoucherGame,
  Telkom,
  GasAlam,
  Bpjs,
  AngsuranKredit,
  TagihanListrik,
  Pdam,
  InternetTv,
  Pascabayar,
  DtlTransactionService,
  SuccessTransactionService,
};
