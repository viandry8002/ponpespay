import axios from 'axios';
import React, {useState} from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../../Styles/Colors';
import {key, postForget} from '../../../../Variable/ApiClient';
import AlertMsg from '../../../Compenent/AlertMsg';
import BtnPrimary from '../../../Compenent/BtnPrimary';
import FormInput from '../../../Compenent/FormInput';
import SpinnerLoad from '../../../Compenent/SpinnerLoad';

const EmailForgot = ({route, navigation}) => {
  const [load, setLoad] = useState(false);
  const [email, setEmail] = useState('');

  const submit = () => {
    setLoad(true);
    axios
      .post(postForget, {email: email})
      .then(res => {
        res.data.status
          ? navigation.navigate('OtpVerif', {
              type: 'forgot',
              data: {email: email},
            })
          : AlertMsg(res.data.message);
        setLoad(false);
      })
      .catch(err => {
        console.log(err);
        setLoad(false);
      });
  };

  return (
    <SafeAreaView style={styles.wrap}>
      <SpinnerLoad loads={load} />
      <FormInput
        title={'Email'}
        placholder={'Masukan email'}
        icon={
          <MaterialCommunityIcons
            name="email"
            size={20}
            color={Colors.primary1}
          />
        }
        val={email}
        change={value => setEmail(value)}
      />
      <BtnPrimary
        title={'Minta Kode Verifikasi'}
        pressed={() => submit()}
        // login
        // pressed={() => navigation.navigate('Home')}
      />
    </SafeAreaView>
  );
};

export default EmailForgot;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 16,
  },
});
