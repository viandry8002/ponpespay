import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Colors from '../../../../../Styles/Colors';
import {screenWidth} from '../../../../../Styles/Styles';
import Font from '../../../../../Styles/Fonts';

const game = [
  {
    id: 1,
    title: 'Pubg Mobile',
    image: require('../../../../../Assets/images/pubg.png'),
  },
  {
    id: 2,
    title: 'Hago',
    image: require('../../../../../Assets/images/hago.png'),
  },
  {
    id: 3,
    title: 'Pubg Mobile',
    image: require('../../../../../Assets/images/pubg.png'),
  },
  {
    id: 4,
    title: 'Hago',
    image: require('../../../../../Assets/images/hago.png'),
  },
];

const JenisVoucherGame = () => {
  const [refresh, setRefresh] = useState(false);

  const onRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      setRefresh(false);
    }, 1000);
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        // width: screenWidth - 210,
        flex: 0.5,
        height: 110,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: Colors.container,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 4,
      }}>
      <Image
        source={item.image}
        style={{width: 56, height: 56, marginBottom: 8}}
        resizeMode={'contain'}
      />
      <Text style={Font.text}>{item.title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: Colors.white,
      }}>
      <FlatList
        key={'#'}
        keyExtractor={item => '#' + item.id}
        data={game}
        renderItem={renderItem}
        style={{marginVertical: 4}}
        numColumns={2}
        refreshControl={
          <RefreshControl
            refreshing={refresh}
            onRefresh={onRefresh}
            colors={[Colors.primary1]}
          />
        }
      />
    </SafeAreaView>
  );
};

export default JenisVoucherGame;

const styles = StyleSheet.create({});
