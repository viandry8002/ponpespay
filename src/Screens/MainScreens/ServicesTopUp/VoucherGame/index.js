import React, {useEffect, useState} from 'react';
import {
  FlatList,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {screenWidth, screenWidthMargin} from '../../../../Styles/Styles';
import BottomService1 from '../../../Compenent/BottomService1';
import FormatMoney from '../../../Compenent/FormatMoney';
import FormInput from '../../../Compenent/FormInput';
import Gradient from '../../../Compenent/Gradient';

const voucher = [
  {
    id: 1,
    title: '60 UC',
    cost: 5850,
  },
  {
    id: 2,
    title: '300+25 UC',
    cost: 15850,
  },
  {
    id: 3,
    title: '1500+300 UC',
    cost: 25850,
  },
  {
    id: 4,
    title: '3000+850 UC',
    cost: 25850,
  },
  {
    id: 5,
    title: '6000+21000 UC',
    cost: 25850,
  },
];

const VoucherGame = ({route, navigation}) => {
  const [selected, setSelected] = useState(1);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {}, []);

  const onRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      setRefresh(false);
    }, 1000);
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        // width: 165,
        height: 48,
        borderColor: item.id === selected ? Colors.primary1 : Colors.container,
        borderWidth: 1,
        marginHorizontal: 16,
        paddingHorizontal: 16,
        marginVertical: 4,
        borderRadius: 10,
        // alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:
          item.id === selected ? Colors.primary1shading : Colors.white,
      }}
      onPress={() => setSelected(item.id)}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text style={Font.subtitle}>{item.title}</Text>
        <FormatMoney
          value={item.cost}
          style={[Font.bold, {color: Colors.green2}]}
        />
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Gradient
        style={{height: 56, position: 'absolute', width: screenWidth}}
      />
      <View style={{flex: 1}}>
        {/* float tlp */}
        <View style={[styles.wrapFloatSaldo]}>
          <FormInput
            title={'Jenis Voucher Game'}
            placholder={'Pilih Jenis Voucher Game'}
            rightIcon={
              <FontAwesome5
                name="chevron-down"
                size={16}
                color={Colors.placeholder}
                onPress={() => navigation.navigate('JenisVoucherGame')}
              />
            }
            // val={'0812345670982'}
            bottomZero={true}
          />
        </View>
        <View style={{height: 10}} />

        {/* body */}
        <FlatList
          key={'#'}
          keyExtractor={item => '#' + item.id}
          data={voucher}
          renderItem={renderItem}
          numColumns={1}
          refreshControl={
            <RefreshControl
              refreshing={refresh}
              onRefresh={onRefresh}
              colors={[Colors.primary1]}
            />
          }
        />
      </View>

      {/* footer */}
      <BottomService1
        balance={'100000000'}
        total={'5850'}
        pressed={() =>
          navigation.navigate('SuccessTransactionService', {
            type: {
              icon: require('../../../../Assets/icons/vgame.png'),
              title: 'PUBG Mobile - 60 UC',
              titleNumber: 'No Voucher Google Play',
              number: '32323232332323',
            },
            data: {
              Status: 'Diproses',
              Waktu_Transaksi: '12 Des 2021 , 09:20',
              Invoice: '102381082421',
              Metode_Pembayaran: 'Saldo Dompet',
              Kategori_Produk: 'Voucher Game',
              Jenis_Layanan: 'PUBG Mobile - 60 UC',
              Harga: '5850',
              Total_Pembayaran: '5850',
            },
          })
        }
      />
    </SafeAreaView>
  );
};

export default VoucherGame;

const styles = StyleSheet.create({
  wrapFloatSaldo: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    alignSelf: 'center',
    padding: 16,
    borderRadius: 15,
    marginTop: 16,
    shadowColor: 'grey',
    elevation: 10,
  },
});
