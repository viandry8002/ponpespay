import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {screenWidth, screenWidthMargin} from '../../../../Styles/Styles';
import {key} from '../../../../Variable/ApiClient';
import BottomService1 from '../../../Compenent/BottomService1';
import FormatMoney from '../../../Compenent/FormatMoney';
import FormInput from '../../../Compenent/FormInput';
import Gradient from '../../../Compenent/Gradient';

const pulsa = [
  {
    id: 1,
    title: 5000,
    cost: 5850,
  },
  {
    id: 2,
    title: 10000,
    cost: 10850,
  },
  {
    id: 3,
    title: 15000,
    cost: 15850,
  },
  {
    id: 4,
    title: 20000,
    cost: 20850,
  },
  {
    id: 5,
    title: 25000,
    cost: 25850,
  },
];

const paket = [
  {
    id: 1,
    title: 'Paket Data 10 GB',
    desc: '5 Gb Internet + 5 Gb Sosial Media Berlaku hingga 1 bulan',
    cost: 5850,
  },
  {
    id: 2,
    title: 'Paket Data 10 GB',
    desc: '5 Gb Internet + 5 Gb Sosial Media Berlaku hingga 1 bulan',
    cost: 10850,
  },
  {
    id: 3,
    title: 'Paket Data 10 GB',
    desc: '5 Gb Internet + 5 Gb Sosial Media Berlaku hingga 1 bulan',
    cost: 15850,
  },
];

const PulsaPaket = ({route, navigation}) => {
  const [ActiveTop, setActiveTop] = useState(1);
  const [selected, setSelected] = useState(1);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    navigation.setOptions({title: route.params.title});
    navigateMenu(route.params.id ? route.params.id : ActiveTop);
  }, []);

  const onRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      setRefresh(false);
    }, 1000);
  };

  const navigateMenu = active => {
    setActiveTop(active);
    switch (active) {
      case 1:
        navigation.setOptions({title: 'Pulsa'});
        break;
      default:
        navigation.setOptions({title: 'Paket Data'});
        break;
    }
  };

  const activeMenu = (title, active) => (
    <TouchableOpacity
      style={{flex: 1, justifyContent: 'space-between'}}
      onPress={() => navigateMenu(active)}>
      <Text
        style={[Font.subtitle, {color: Colors.primary1, textAlign: 'center'}]}>
        {title}
      </Text>
      <View
        style={{
          borderBottomColor: Colors.primary1,
          borderBottomWidth: 2,
        }}
      />
    </TouchableOpacity>
  );

  const inactiveMenu = (title, active) => (
    <TouchableOpacity
      style={{flex: 1, justifyContent: 'space-between'}}
      onPress={() => navigateMenu(active)}>
      <Text
        style={[
          Font.subtitle2,
          {textAlign: 'center', color: Colors.placeholder},
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );

  const renderItem1 = ({item}) => (
    <TouchableOpacity
      style={{
        width: 165,
        height: 75,
        borderColor: item.id === selected ? Colors.primary1 : Colors.container,
        borderWidth: 1,
        margin: 8,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:
          item.id === selected ? Colors.primary1shading : Colors.white,
      }}
      onPress={() => setSelected(item.id)}>
      <FormatMoney value={item.title} style={Font.titleLarge2} />
      <Text style={[Font.bold, {color: Colors.green2}]}>
        Harga : <FormatMoney value={item.cost} />
      </Text>
    </TouchableOpacity>
  );

  const renderItem2 = ({item}) => (
    <TouchableOpacity
      style={{
        // width: 165,
        height: 69,
        borderColor: item.id === selected ? Colors.primary1 : Colors.container,
        borderWidth: 1,
        marginHorizontal: 16,
        paddingHorizontal: 16,
        marginVertical: 4,
        borderRadius: 10,
        // alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:
          item.id === selected ? Colors.primary1shading : Colors.white,
      }}
      onPress={() => setSelected(item.id)}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginBottom: 8,
        }}>
        <Text style={Font.subtitle}>{item.title}</Text>
        <FormatMoney
          value={item.cost}
          style={[Font.bold, {color: Colors.green2}]}
        />
      </View>
      <Text style={[Font.caption, {color: Colors.placeholder}]}>
        {item.desc}
      </Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Gradient
        style={{height: 56, position: 'absolute', width: screenWidth}}
      />
      <View style={{flex: 1}}>
        {/* float tlp */}
        <View style={[styles.wrapFloatSaldo]}>
          <FormInput
            title={'Nomor Telpon'}
            placholder={'Masukan nomor telpon'}
            rightIcon={
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../../../Assets/icons/telkomsel.png')}
                  style={{width: 60, height: 24, marginRight: 16}}
                  resizeMode={'contain'}
                />
                <Image
                  source={require('../../../../Assets/icons/contact.png')}
                  style={{width: 24, height: 24}}
                  resizeMode={'contain'}
                />
              </View>
            }
            val={'0812345670982'}
            bottomZero={true}
          />
        </View>

        <View style={{height: 16}} />
        {/* float menu */}
        <View style={{overflow: 'hidden', paddingBottom: 10}}>
          <View
            style={{
              height: 32,
              elevation: 5,
              shadowColor: 'grey',
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            {ActiveTop === 1
              ? activeMenu('Pulsa', 1)
              : inactiveMenu('Pulsa', 1)}
            {ActiveTop === 1
              ? inactiveMenu('Paket Data', 2)
              : activeMenu('Paket Data', 2)}
          </View>
        </View>

        {/* body */}
        {ActiveTop === 1 ? (
          <FlatList
            key={'_'}
            keyExtractor={item => '_' + item.id}
            data={pulsa}
            renderItem={renderItem1}
            style={{alignSelf: 'center'}}
            numColumns={2}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
                colors={[Colors.primary1]}
              />
            }
          />
        ) : (
          <FlatList
            key={'#'}
            keyExtractor={item => '#' + item.id}
            data={paket}
            renderItem={renderItem2}
            style={{marginVertical: 12}}
            numColumns={1}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
                colors={[Colors.primary1]}
              />
            }
          />
        )}
      </View>

      {/* footer */}
      <BottomService1
        balance={'100000000'}
        total={'5850'}
        pressed={() =>
          navigation.navigate('SuccessTransactionService', {
            type: {
              icon: require('../../../../Assets/icons/pulsa.png'),
              title: 'Telkomsel - Simpati 5.000',
              titleNumber: false,
              number: '08923812125124',
            },
            data: {
              Status: 'Diproses',
              Waktu_Transaksi: '12 Des 2021 , 09:20',
              Invoice: '102381082421',
              Metode_Pembayaran: 'Saldo Dompet',
              Kategori_Produk: 'Pulsa',
              Jenis_Layanan: 'Telkomsel 5.000',
              Nomor: '081231231412',
              Harga: '5850',
              Total_Pembayaran: '5850',
            },
          })
        }
      />
      {/* <BottomService2 /> */}
    </SafeAreaView>
  );
};

export default PulsaPaket;

const styles = StyleSheet.create({
  btnCheckout: {
    width: 165,
    height: 48,
    backgroundColor: Colors.primary1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  wrapFloatSaldo: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    // height: 94,
    alignSelf: 'center',
    padding: 16,
    borderRadius: 15,
    marginTop: 16,
    shadowColor: 'grey',
    elevation: 10,
  },
});
