import React from 'react';
import {Image, SafeAreaView, StyleSheet, View} from 'react-native';
import Colors from '../../../../Styles/Colors';
import {screenWidth, screenWidthMargin} from '../../../../Styles/Styles';
import BottomService2 from '../../../Compenent/BottomService2';
import FormInput from '../../../Compenent/FormInput';
import Gradient from '../../../Compenent/Gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import BottomService1 from '../../../Compenent/BottomService1';

const TagihanListrik = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Gradient
        style={{height: 56, position: 'absolute', width: screenWidth}}
      />
      <View style={{flex: 1}}>
        <View style={[styles.wrapFloatSaldo]}>
          <FormInput
            title={'Jenis Produk Listrik'}
            placholder={'Pilih Jenis Produk Listrik'}
            rightIcon={
              <FontAwesome5
                name="chevron-down"
                size={16}
                color={Colors.placeholder}
                // onPress={() => navigation.navigate('JenisVoucherGame')}
              />
            }
            bottomZero={true}
          />
          <FormInput
            title={'No. Meter/ID Pelanggan'}
            placholder={'Masukan No. Meter/ID Pelanggan'}
            bottomZero={true}
          />
        </View>
        <View style={{height: 10}} />

        {/* body */}
      </View>

      {/* footer */}
      <BottomService1
        balance={'100000000'}
        total={'5850'}
        pressed={() =>
          navigation.navigate('DtlTransactionService', {
            type: {
              icon: require('../../../../Assets/icons/listrik.png'),
              title: 'Listrik PLN',
              titleNumber: false,
              number: '08923812125124',
            },
            data: {
              Status: 'Diproses',
              Waktu_Transaksi: '12 Des 2021 , 09:20',
              Invoice: '102381082421',
              Metode_Pembayaran: 'Saldo Dompet',
              Kategori_Produk: 'Listrik PLN',

              Jenis_Layanan: 'Tagihan Listrik',
              Tarif_Daya: 'R1 / 450',
              Bulan_Tahun: 'JAN23',
              ID_Pelanggan: '081231231412',
              Nama_Pelanggan: 'Aditiya Permana',

              Tagihan: '100000',
              Biaya_Admin: '2500',
              Total_Pembayaran: '102500',
            },
          })
        }
      />
    </SafeAreaView>
  );
};

export default TagihanListrik;

const styles = StyleSheet.create({
  wrapFloatSaldo: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    alignSelf: 'center',
    padding: 16,
    borderRadius: 15,
    marginTop: 16,
    shadowColor: 'grey',
    elevation: 10,
  },
});
