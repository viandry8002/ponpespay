import React from 'react';
import {Image, SafeAreaView, StyleSheet, View} from 'react-native';
import Colors from '../../../../Styles/Colors';
import {screenWidth, screenWidthMargin} from '../../../../Styles/Styles';
import BottomService2 from '../../../Compenent/BottomService2';
import FormInput from '../../../Compenent/FormInput';
import Gradient from '../../../Compenent/Gradient';

const Telkom = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Gradient
        style={{height: 56, position: 'absolute', width: screenWidth}}
      />
      <View style={{flex: 1}}>
        <View style={[styles.wrapFloatSaldo]}>
          <FormInput
            title={'Nomor Telpon'}
            placholder={'Masukan Nomor Telpon'}
            rightIcon={
              <Image
                source={require('../../../../Assets/icons/indihome.png')}
                style={{width: 60, height: 24, marginRight: 16}}
                resizeMode={'contain'}
              />
            }
            bottomZero={true}
          />
        </View>
        <View style={{height: 10}} />

        {/* body */}
      </View>

      {/* footer */}
      <BottomService2
        balance={'100000000'}
        pressed={() =>
          navigation.navigate('DtlTransactionService', {
            type: {
              icon: require('../../../../Assets/icons/telkom.png'),
              title: 'Telkom',
              titleNumber: false,
              number: '08923812125124',
            },
            data: {
              Status: 'Diproses',
              Waktu_Transaksi: '12 Des 2021 , 09:20',
              Invoice: '102381082421',
              Metode_Pembayaran: 'Saldo Dompet',
              Kategori_Produk: 'Telkom',
              Nomor_Telepon: '081231231412',
              Nama_Pelanggan: 'Aditiya Permana',
              Tagihan: '100000',
              Biaya_Admin: '2500',
              Total_Pembayaran: '102500',
            },
          })
        }
      />
    </SafeAreaView>
  );
};

export default Telkom;

const styles = StyleSheet.create({
  wrapFloatSaldo: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    // height: 94,
    alignSelf: 'center',
    padding: 16,
    borderRadius: 15,
    marginTop: 16,
    shadowColor: 'grey',
    elevation: 10,
  },
});
