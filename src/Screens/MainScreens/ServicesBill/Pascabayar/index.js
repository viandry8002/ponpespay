import React from 'react';
import {Image, SafeAreaView, StyleSheet, View} from 'react-native';
import Colors from '../../../../Styles/Colors';
import {screenWidth, screenWidthMargin} from '../../../../Styles/Styles';
import BottomService2 from '../../../Compenent/BottomService2';
import FormInput from '../../../Compenent/FormInput';
import Gradient from '../../../Compenent/Gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import BottomService1 from '../../../Compenent/BottomService1';

const Pascabayar = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Gradient
        style={{height: 56, position: 'absolute', width: screenWidth}}
      />
      <View style={{flex: 1}}>
        <View style={[styles.wrapFloatSaldo]}>
          <FormInput
            title={'Pilih Operator'}
            placholder={'Pilih Operator'}
            rightIcon={
              <FontAwesome5
                name="chevron-down"
                size={16}
                color={Colors.placeholder}
                // onPress={() => navigation.navigate('JenisVoucherGame')}
              />
            }
            bottomZero={true}
          />
          <FormInput
            title={'Nomor Telpon'}
            placholder={'Masukan Nomor Telpon'}
            bottomZero={true}
          />
        </View>
        <View style={{height: 10}} />

        {/* body */}
      </View>

      {/* footer */}
      <BottomService1
        balance={'100000000'}
        total={'5850'}
        pressed={() =>
          navigation.navigate('DtlTransactionService', {
            type: {
              icon: require('../../../../Assets/icons/pascabayar.png'),
              title: 'Pascabayar',
              titleNumber: false,
              number: '08923812125124',
            },
            data: {
              Status: 'Diproses',
              Waktu_Transaksi: '12 Des 2021 , 09:20',
              Invoice: '102381082421',
              Metode_Pembayaran: 'Saldo Dompet',
              Kategori_Produk: 'Pascabayar',

              Jenis_Layanan: 'Telkomsel - Halo',
              Bulan_Tahun: 'JUN23',
              Nama_Pelanggan: 'Aditiya Permana',
              Nomor_Telepon: '081231231412',

              Tagihan: '100000',
              Biaya_Admin: '2500',
              Total_Pembayaran: '102500',
            },
          })
        }
      />
    </SafeAreaView>
  );
};

export default Pascabayar;

const styles = StyleSheet.create({
  wrapFloatSaldo: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    alignSelf: 'center',
    padding: 16,
    borderRadius: 15,
    marginTop: 16,
    shadowColor: 'grey',
    elevation: 10,
  },
});
