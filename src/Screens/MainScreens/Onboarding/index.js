import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Onboarding = ({navigation}) => {
  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="transparent" translucent />
      <LinearGradient
        start={{x: 0.0, y: 0.5}}
        end={{x: 1.0, y: 0.5}}
        colors={[Colors.primary1, Colors.primary2]}
        style={{
          flex: 1,
          paddingHorizontal: 16,
        }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={require('../../../Assets/images/imgOnboard.png')}
            style={{width: 326, height: 250}}
          />
          <View style={{height: 16}} />
          <Text style={[Font.title32Large, {color: Colors.white}]}>
            Semua Pembayaran{'\n'} Jadi Lebih Mudah
          </Text>
          <Text style={[Font.subtitle2, {color: Colors.white}]}>
            Pospespay menyediakan layanan PPOB untuk{'\n'} memudahkan kebutuhan
            transaksi sehari hari
          </Text>
        </View>
        <TouchableOpacity
          style={styles.btnBtm}
          onPress={() => navigation.navigate('Auth', {type: 'Login'})}>
          <Text style={[Font.subtitle, {color: Colors.primary1}]}>
            Mulai Sekarang
          </Text>
          <AntDesign name="rightcircleo" size={20} color={Colors.primary1} />
        </TouchableOpacity>
      </LinearGradient>
    </View>
  );
};

export default Onboarding;

const styles = StyleSheet.create({
  btnBtm: {
    height: 56,
    backgroundColor: Colors.white,
    borderRadius: 100,
    marginBottom: 32,
    paddingHorizontal: 24,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
