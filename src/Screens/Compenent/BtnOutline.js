import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';

const BtnOutline = ({title, pressed, leftIcon, color}) => {
  return (
    <TouchableOpacity
      style={{
        height: 40,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: color ? color : Colors.primary1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
      }}
      onPress={pressed}>
      {leftIcon}
      <Text style={[Font.subtitle, {color: color ? color : Colors.primary1}]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default BtnOutline;

const styles = StyleSheet.create({});
