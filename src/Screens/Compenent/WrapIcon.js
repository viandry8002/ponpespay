import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import Colors from '../../Styles/Colors'

const WrapIcon = ({ icon }) => {
    return (
        <View style={[{ width: 46, height: 46, backgroundColor: Colors.white, borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginBottom: 8 }, styles.shadow]}>
            <Image source={icon} style={{ width: 36, height: 36 }} />
        </View>
    )
}

export default WrapIcon

const styles = StyleSheet.create({
    shadow: {
        shadowColor: 'grey',
        elevation: 3,
    }
})