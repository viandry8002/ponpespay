import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {TextMask} from 'react-native-masked-text';

const FormatMoney = ({value, line, style}) => {
  return (
    <TextMask
      type={'money'}
      options={{
        precision: 0,
        separator: '.',
        delimiter: '.',
        unit: 'Rp',
        suffixUnit: '',
      }}
      value={value}
      numberOfLines={line ? line : 1}
      style={style}
    />
  );
};

export default FormatMoney;

const styles = StyleSheet.create({});
