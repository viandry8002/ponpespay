import {StyleSheet, Text, View, Alert} from 'react-native';
import React from 'react';

const AlertMsg = msg => {
  Alert.alert('Peringatan', msg, [
    {text: 'OK', onPress: () => console.log('OK')},
  ]);
};

export default AlertMsg;

const styles = StyleSheet.create({});
