import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import BtnPrimary from './BtnPrimary';
import Font from '../../Styles/Fonts';
import Colors from '../../Styles/Colors';
import FormatMoney from './FormatMoney';

const BottomService2 = ({balance, pressed}) => {
  return (
    <View
      style={{
        backgroundColor: Colors.white,
        // height: 80,
        // flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems: 'center',
        padding: 16,
        elevation: 15,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={require('../../Assets/icons/Wallet_fill.png')}
            style={{width: 18, height: 18}}
          />
          <Text style={[Font.text, {color: Colors.placeholder, marginLeft: 4}]}>
            Saldo
          </Text>
        </View>
        <FormatMoney value={balance} style={[Font.titleLarge]} />
      </View>
      <View style={{height: 8}} />
      {/* active */}
      <BtnPrimary title={'Bayar'} pressed={pressed} />
      {/* nonactive */}
      {/* <BtnPrimary title={'Bayar'} bgColor={Colors.gray4} /> */}
    </View>
  );
};

export default BottomService2;

const styles = StyleSheet.create({});
