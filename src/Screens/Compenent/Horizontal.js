import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../Styles/Colors'

const Horizontal = () => {
    return (
        <View
            style={{
                borderBottomColor: Colors.container,
                borderBottomWidth: 1,
            }}
        />
    )
}

export default Horizontal