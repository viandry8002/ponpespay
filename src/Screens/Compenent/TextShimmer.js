import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder'
import LinearGradient from 'react-native-linear-gradient';

const TextShimmer = ({ type }) => {
    return (
        <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={type === 'large' ? styles.large : (type === 'small' ? styles.small : styles.normal)}
        />
    )
}

export default TextShimmer

const styles = StyleSheet.create({
    small: { borderRadius: 100, width: 50, height: 14 },
    normal: { borderRadius: 100, width: 55, height: 18, },
    large: { borderRadius: 100, width: 125, height: 21, },
})