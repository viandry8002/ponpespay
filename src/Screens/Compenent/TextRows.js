import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Font from '../../Styles/Fonts';
import Colors from '../../Styles/Colors';

const TextRows = ({text1, text2, right}) => {
  return (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text
        style={[
          Font.bold,
          right === true ? {flex: 1, textAlign: 'right'} : {flex: 1},
        ]}>
        {text2}
      </Text>
    </View>
  );
};

export default TextRows;

const styles = StyleSheet.create({
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
});
