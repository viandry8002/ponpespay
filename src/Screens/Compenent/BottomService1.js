import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import BtnPrimary from './BtnPrimary';
import Font from '../../Styles/Fonts';
import Colors from '../../Styles/Colors';
import FormatMoney from './FormatMoney';

const BottomService1 = ({balance, total, pressed}) => {
  return (
    <View style={styles.wrapLayer1}>
      <View style={styles.wrapLayer2}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={require('../../Assets/icons/Wallet_fill.png')}
            style={{width: 18, height: 18}}
          />
          <Text style={[Font.text, {color: Colors.placeholder, marginLeft: 4}]}>
            Saldo
          </Text>
        </View>
        <FormatMoney value={balance} style={[Font.titleLarge]} />
      </View>
      <View style={{height: 8}} />
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <View style={{flex: 1}}>
          <Text style={Font.text}>Total Harga :</Text>
          <FormatMoney
            value={total}
            style={[Font.titleLarge, {color: Colors.primary1}]}
          />
        </View>
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          {/* active */}
          <View style={{width: 160}}>
            <BtnPrimary
              title={'Bayar'}
              pressed={pressed}
              //   pressed={() =>
              //     navigation.navigate('SuccessTransactionService', {
              //       rangkuman_pesanan: (
              //         <View>
              //           {textRows('Kategori Produk', 'Pulsa')}
              //           {textRows('Jenis Layanan', 'Telkomsel 5.000')}
              //           {textRows('Nomor', '081231231412')}
              //           {textRows('Harga', <FormatMoney value={'5850'} />)}
              //         </View>
              //       ),
              //     })
              //   }
            />
          </View>
          {/* nonactive */}
          {/* <View style={{width: 160}}>
              <BtnPrimary title={'Bayar'} bgColor={Colors.gray4} />
            </View> */}
        </View>
      </View>
    </View>
  );
};

export default BottomService1;

const styles = StyleSheet.create({
  wrapLayer1: {
    backgroundColor: Colors.white,
    justifyContent: 'space-between',
    padding: 16,
    elevation: 15,
  },
  wrapLayer2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
