import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Colors from '../../Styles/Colors';

const SpinnerLoad = ({loads}) => {
  console.log(loads);
  return (
    <Spinner
      visible={loads}
      textContent={'Loading...'}
      textStyle={{color: Colors.white, fontSize: 12}}
    />
  );
};

export default SpinnerLoad;
