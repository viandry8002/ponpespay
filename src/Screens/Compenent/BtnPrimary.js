import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';

const BtnPrimary = ({
  title,
  pressed,
  leftIcon,
  bgColor,
  colorTint,
  customText,
}) => {
  return (
    <TouchableOpacity
      style={{
        height: 40,
        backgroundColor: bgColor ? bgColor : Colors.primary1,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
      }}
      disabled={pressed ? false : true}
      onPress={pressed}>
      {leftIcon}
      {customText ? (
        customText
      ) : (
        <Text
          style={[
            Font.subtitle,
            {color: colorTint ? colorTint : Colors.white},
          ]}>
          {title}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default BtnPrimary;

const styles = StyleSheet.create({});
