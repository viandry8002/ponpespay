import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';
import FormatMoney from './FormatMoney';
import WrapIcon from './WrapIcon';
import Feather from 'react-native-vector-icons/Feather';

const FloatService = ({icon, title, titleNumber, number}) => {
  return (
    <>
      {titleNumber ? (
        <View style={styles.wrap}>
          <View style={styles.rowCenter}>
            <WrapIcon icon={icon} />
            <View style={{marginLeft: 8, flex: 1}}>
              <Text style={Font.bold}>{title}</Text>
            </View>
          </View>
          <Text style={[Font.text, {color: Colors.placeholder}]}>
            {titleNumber}
          </Text>
          <TouchableOpacity style={styles.wrapCopy}>
            <Text style={Font.title}>{number} </Text>
            <Feather name="copy" size={18} color={Colors.black} />
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.wrap}>
          <View style={styles.rowCenter}>
            <WrapIcon icon={icon} />
            <View style={{marginLeft: 8}}>
              <Text style={Font.bold}>{title}</Text>
              <Text style={[Font.caption, {color: Colors.placeholder}]}>
                {number}
              </Text>
            </View>
          </View>
        </View>
      )}
    </>
  );
};

export default FloatService;

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: Colors.white,
    borderRadius: 10,
    padding: 8,
    shadowColor: 'grey',
    elevation: 10,
  },
  wrapCopy: {flexDirection: 'row', alignItems: 'center', flex: 1},
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
});
