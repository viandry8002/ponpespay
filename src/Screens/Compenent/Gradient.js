import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../Styles/Colors';

const Gradient = ({style}) => {
  return (
    <LinearGradient
      colors={[Colors.primary1, Colors.primary2]}
      start={{x: 0.0, y: 0.5}}
      end={{x: 1.0, y: 0.5}}
      style={style}
    />
  );
};

export default Gradient;

const styles = StyleSheet.create({});
