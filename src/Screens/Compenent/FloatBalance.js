import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {TextMask} from 'react-native-masked-text';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import {screenWidthMargin} from '../../Styles/Styles';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';

const FloatBalance = ({type, btnTopup, btnHistory, balance}) => {
  return (
    <View
      style={type === 'Home' ? styles.wrapFloatSaldo1 : styles.wrapFloatSaldo2}>
      <View style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 8,
            alignItems: 'center',
          }}>
          <Image
            source={require('../../Assets/icons/wallet.png')}
            style={{width: 20, height: 20, marginRight: 4}}
          />
          <Text style={[Font.text, {color: Colors.placeholder}]}>Saldo</Text>
        </View>
        <TextMask
          type={'money'}
          options={{
            precision: 0,
            separator: '.',
            delimiter: '.',
            unit: 'Rp',
            suffixUnit: '',
          }}
          value={balance}
          style={Font.titleLarge}
          numberOfLines={1}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          flex: 1,
        }}>
        <TouchableOpacity
          style={styles.btnFloatSaldo}
          onPress={btnTopup}
          //   onPress={() => navigation.navigate('TopUp')}
        >
          <View style={styles.wrapBtnSaldo}>
            <AntDesign name="plus" size={20} color={Colors.primary1} />
          </View>
          <Text style={Font.caption}>Top Up</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnFloatSaldo}
          onPress={btnHistory}
          //   onPress={() => navigation.navigate('HistoryTU')}
        >
          <View style={styles.wrapBtnSaldo}>
            <Octicons name="history" size={20} color={Colors.primary1} />
          </View>
          <Text style={Font.caption}>History</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FloatBalance;

const styles = StyleSheet.create({
  wrapFloatSaldo1: {
    backgroundColor: Colors.white,
    width: screenWidthMargin,
    height: 94,
    alignSelf: 'center',
    paddingHorizontal: 16,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: 'grey',
    elevation: 15,
  },
  wrapFloatSaldo2: {
    backgroundColor: Colors.white,
    alignSelf: 'center',
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',

    borderRadius: 8,
    height: 70,
    borderWidth: 1,
    borderColor: Colors.container,
  },
  btnFloatSaldo: {alignItems: 'center', justifyContent: 'space-around'},
  wrapBtnSaldo: {
    width: 42,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.container,
    borderRadius: 15,
    borderWidth: 1,
    marginBottom: 8,
  },
});
