import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Input} from '@rneui/themed';
import Font from '../../Styles/Fonts';
import Colors from '../../Styles/Colors';

const FormInput = ({
  title,
  placholder,
  icon,
  lefticon,
  rightIcon,
  scureText,
  titleCustom,
  val,
  change,
  styled,
  disabled,
  bottomZero,
  inputfocus,
  press,
  tkeyboard,
  errorMsg,
  errorMsgStyle,
}) => {
  return (
    <View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        {icon}
        {titleCustom ? (
          titleCustom
        ) : (
          <Text style={[Font.bold, {marginLeft: 8}]}>{title}</Text>
        )}
      </View>
      <Input
        value={val}
        onChangeText={change}
        label={false}
        onPressIn={press}
        placeholder={placholder}
        style={styled ? styled : [Font.text, {color: Colors.placholder}]}
        // style={{marginBottom: 0, paddingBottom: 0}}
        inputContainerStyle={{
          borderColor: Colors.gray6,
          marginHorizontal: 0,

          // bottom: 0,
          // backgroundColor: 'red',
          // marginBottom: -16,
        }}
        errorMessage={errorMsg ? errorMsg : false}
        errorStyle={
          errorMsgStyle ? errorMsgStyle : bottomZero ? {height: 0} : false
        }
        containerStyle={icon ? {paddingHorizontal: 0} : false}
        inputStyle={{paddingHorizontal: 0}}
        leftIcon={lefticon}
        rightIcon={rightIcon}
        secureTextEntry={scureText}
        showSoftInputOnFocus={inputfocus}
        keyboardType={tkeyboard}
        disabled={disabled}
      />
    </View>
  );
};

export default FormInput;

const styles = StyleSheet.create({});
