import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useState, useEffect} from 'react';
import {
  SplashScreen,
  Onboarding,
  Auth,
  EmailForgot,
  OtpVerif,
  FormAddress,
  FormPassword,
  Success,
  Home,
  TopUp,
  CheckoutTU,
  SuccessTU,
  HistoryTU,
  DtlTransactionTU,
  Transaction,
  DtlTransaksi,
  DtlTransaksiProduct,
  Product,
  Checkout,
  PaymentMethod,
  SuccessCheckoutProduct,
  Profile,
  EditProfile,
  PulsaPaket,
  VoucherGame,
  JenisVoucherGame,
  Telkom,
  GasAlam,
  Bpjs,
  AngsuranKredit,
  TagihanListrik,
  Pdam,
  InternetTv,
  Pascabayar,
  DtlTransactionService,
  SuccessTransactionService,
} from './MainScreens';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Colors from '../Styles/Colors';
import LinearGradient from 'react-native-linear-gradient';
import {screenWidth} from '../Styles/Styles';
import Font from '../Styles/Fonts';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const styleHead = title => ({
  title: title,
  headerTitleStyle: [Font.subtitle, {color: Colors.white}],
  headerTintColor: Colors.white,
  headerBackground: () => (
    <LinearGradient
      colors={[Colors.primary1, Colors.primary2]}
      start={{x: 0.0, y: 0.5}}
      end={{x: 1.0, y: 0.5}}
      style={{height: '100%', width: screenWidth}}
    />
  ),
});

const HomeMenu = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="HomeMenu"
        component={Home}
        options={styleHead('PonpesPay')}
      />
    </Stack.Navigator>
  );
};

const TransactionMenu = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="TransactionMenu"
        component={Transaction}
        options={styleHead('History Transaksi')}
      />
    </Stack.Navigator>
  );
};

const ProductMenu = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ProductMenu"
        component={Product}
        // options={styleHead('Product')}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const ProfilMenu = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ProfilMenu"
        component={Profile}
        options={styleHead('Profile')}
      />
    </Stack.Navigator>
  );
};

const MainTabNavigation = () => {
  return (
    <Tab.Navigator
      // initialRouteName={'Akun'}
      initialRouteName={'HomeSS'}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconColor;
          // let ukuran;

          if (route.name === 'HomeSS') {
            iconColor = focused ? Colors.primary1 : Colors.gray4;
            return (
              <Image
                source={require('../Assets/icons/home.png')}
                style={[styles.navBottomIcon, {tintColor: iconColor}]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'TransactionSS') {
            iconColor = focused ? Colors.primary1 : Colors.gray4;
            return (
              <Image
                source={require('../Assets/icons/transaksi.png')}
                style={[styles.navBottomIcon, {tintColor: iconColor}]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'ProductSS') {
            iconColor = focused ? Colors.primary1 : Colors.gray4;
            return (
              <Image
                source={require('../Assets/icons/produk.png')}
                style={[styles.navBottomIcon, {tintColor: iconColor}]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'ProfilSS') {
            iconColor = focused ? Colors.primary1 : Colors.gray4;
            return (
              <Image
                source={require('../Assets/icons/profil.png')}
                style={[styles.navBottomIcon, {tintColor: iconColor}]}
                resizeMode={'contain'}
              />
            );
          }
        },
      })}
      activeColor={Colors.primary1}
      inactiveColor={Colors.gray4}
      barStyle={{
        backgroundColor: Colors.white,
      }}
      shifting={false}>
      <Tab.Screen
        name="HomeSS"
        component={HomeMenu}
        options={{title: 'Home'}}
      />
      <Tab.Screen
        name="TransactionSS"
        component={TransactionMenu}
        options={{title: 'Transaksi'}}
      />
      <Tab.Screen
        name="ProductSS"
        component={ProductMenu}
        options={{title: 'Product'}}
      />
      <Tab.Screen
        name="ProfilSS"
        component={ProfilMenu}
        options={{title: 'Profil'}}
      />
    </Tab.Navigator>
  );
};

const MainNavigation = props => {
  return (
    <Stack.Navigator
      initialRouteName={props.token !== null ? 'Home' : 'Onboarding'}>
      <Stack.Screen
        name="Onboarding"
        component={Onboarding}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Auth"
        component={Auth}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EmailForgot"
        component={EmailForgot}
        options={styleHead('Lupa Password')}
      />
      <Stack.Screen
        name="OtpVerif"
        component={OtpVerif}
        options={styleHead('Verifikasi')}
      />
      <Stack.Screen
        name="FormAddress"
        component={FormAddress}
        options={styleHead('Data Alamat')}
      />
      <Stack.Screen
        name="FormPassword"
        component={FormPassword}
        options={styleHead('Password Baru')}
      />
      <Stack.Screen
        name="Success"
        component={Success}
        options={styleHead('Success')}
      />
      <Stack.Screen
        name="Home"
        component={MainTabNavigation}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="TopUp"
        component={TopUp}
        options={styleHead('Top Up')}
      />
      <Stack.Screen
        name="CheckoutTU"
        component={CheckoutTU}
        options={styleHead('Top Up')}
      />
      <Stack.Screen
        name="SuccessTU"
        component={SuccessTU}
        options={styleHead('Top Up')}
      />
      <Stack.Screen
        name="HistoryTU"
        component={HistoryTU}
        options={styleHead('Detail transaksi')}
      />
      <Stack.Screen
        name="DtlTransactionTU"
        component={DtlTransactionTU}
        options={styleHead('Riwayat Top Up')}
      />
      <Stack.Screen
        name="History"
        component={Transaction}
        options={styleHead('History Transaksi')}
      />
      <Stack.Screen
        name="DtlTransaksi"
        component={DtlTransaksi}
        options={styleHead('Detail transaksi')}
      />
      <Stack.Screen
        name="DtlTransaksiProduct"
        component={DtlTransaksiProduct}
        options={styleHead('Detail transaksi')}
      />
      <Stack.Screen
        name="Checkout"
        component={Checkout}
        options={styleHead('Checkout')}
      />
      <Stack.Screen
        name="PaymentMethod"
        component={PaymentMethod}
        options={styleHead('Pilih metode pembayaran')}
      />
      <Stack.Screen
        name="SuccessCheckoutProduct"
        component={SuccessCheckoutProduct}
        options={styleHead('Checkout')}
      />
      <Stack.Screen
        name="PulsaPaket"
        component={PulsaPaket}
        options={styleHead('Pulsa')}
      />
      <Stack.Screen
        name="VoucherGame"
        component={VoucherGame}
        options={styleHead('Voucher Game')}
      />
      <Stack.Screen
        name="JenisVoucherGame"
        component={JenisVoucherGame}
        options={styleHead('Pilih Jenis Voucher Game')}
      />
      {/* tagihan dan pembayaran */}
      <Stack.Screen
        name="Telkom"
        component={Telkom}
        options={styleHead('Telkom')}
      />
      <Stack.Screen
        name="GasAlam"
        component={GasAlam}
        options={styleHead('Gas Alam')}
      />
      <Stack.Screen name="Bpjs" component={Bpjs} options={styleHead('BPJS')} />
      <Stack.Screen
        name="AngsuranKredit"
        component={AngsuranKredit}
        options={styleHead('Angsuran Kredit')}
      />
      <Stack.Screen
        name="TagihanListrik"
        component={TagihanListrik}
        options={styleHead('Tagihan Listrik')}
      />
      <Stack.Screen name="Pdam" component={Pdam} options={styleHead('PDAM')} />
      <Stack.Screen
        name="InternetTv"
        component={InternetTv}
        options={styleHead('Internet & TV Kabel')}
      />
      <Stack.Screen
        name="Pascabayar"
        component={Pascabayar}
        options={styleHead('Pascabayar')}
      />

      <Stack.Screen
        name="DtlTransactionService"
        component={DtlTransactionService}
        options={styleHead('Cek Detail Transaksi')}
      />
      <Stack.Screen
        name="SuccessTransactionService"
        component={SuccessTransactionService}
        options={styleHead('Transaksi Berhasil')}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={styleHead('Edit Profile')}
      />
    </Stack.Navigator>
  );
};

const Routes = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [token, setToken] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);

    async function getToken() {
      const token = await AsyncStorage.getItem('api_token');
      setToken(token);
    }
    getToken();
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation token={token} />
    </NavigationContainer>
  );
};

export default Routes;

const styles = StyleSheet.create({
  navBottomIcon: {width: 24, height: 24},
});
