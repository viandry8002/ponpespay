import {View, Text, SafeAreaView, Image} from 'react-native';
import React from 'react';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Compenent/BtnPrimary';
import Font from '../../../Styles/Fonts';

const Success = ({navigation}) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: Colors.white,
        justifyContent: 'center',
      }}>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Image
          source={require('../../../Assets/icons/success.png')}
          style={{width: 56, height: 56}}
        />
        <View style={{height: 16}} />
        <Text style={[Font.titleLarge, {color: Colors.success}]}>
          Password Berhasil Di Ubah
        </Text>
      </View>
      <View style={{height: 16}} />
      <View style={{marginHorizontal: 16}}>
        <BtnPrimary
          title={'Login'}
          pressed={() => navigation.navigate('Auth', {type: 'Login'})}
        />
      </View>
    </SafeAreaView>
  );
};

export default Success;
