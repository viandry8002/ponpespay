import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  ScrollView,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Compenent/BtnPrimary';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import axios from 'axios';
import {
  key,
  postCheckOtp,
  postCheckOtpReset,
  postResendRegist,
  postResendReset,
} from '../../../Variable/ApiClient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AlertMsg from '../../Compenent/AlertMsg';
import SpinnerLoad from '../../Compenent/SpinnerLoad';

const OtpVerif = ({route, navigation}) => {
  const [load, setLoad] = useState(false);
  const [form, setForm] = useState({
    email: route.params.data.email,
    otp: '',
  });
  const [count, setcount] = useState(0);
  const [isResend, setIsResend] = useState(false);

  // const navTo = () => {
  //   route.params.type === 'regist'
  //     ? navigation.navigate('RegistAddress', {type: 'auth'})
  //     : navigation.navigate('NewPassword', {type: 'auth'});
  // };

  useEffect(() => {
    countDown(30);
  }, []);

  const countDown = c => {
    setcount(c);
    if (c == 30) {
      let interval = setInterval(() => {
        setcount(prev => {
          setIsResend(false);
          if (prev == 1) clearInterval(interval), setIsResend(true);
          return prev - 1;
        });
      }, 1000);
    }
  };

  const resendToken = () => {
    console.log(route.params.type);
    isResend
      ? axios
          .post(
            route.params.type === 'regist' ? postResendRegist : postResendReset,
            {
              email: route.params.data.email,
            },
          )
          .then(res => {
            countDown(30);
          })
          .catch(err => {
            // console.log('aaaaz', err);
            console.log(err.response.data);
          })
      : false;
  };

  const saveToken = async token => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    setLoad(true);
    axios
      .post(
        route.params.type === 'regist' ? postCheckOtp : postCheckOtpReset,
        form,
      )
      .then(res => {
        res.data.status
          ? route.params.type === 'regist'
            ? (saveToken(res.data.token),
              navigation.navigate('FormAddress', {
                type: 'auth',
                data: route.params.data,
              }))
            : navigation.navigate('FormPassword', {
                type: 'auth',
                data: res.data._id,
                email: route.params.data.email,
                otp: form.otp,
              })
          : AlertMsg(res.data.message);
        setLoad(false);
      })
      .catch(err => {
        console.log(err);
        setLoad(false);
      });
  };

  return (
    <SafeAreaView style={styles.wrap}>
      <SpinnerLoad loads={load} />
      <ScrollView
        style={{flex: 1, padding: 16}}
        showsVerticalScrollIndicator={false}
        e>
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/images/imgOtp.png')}
            style={{width: 250, height: 270}}
          />
          <View style={{height: 16}} />
          <Text style={[Font.titleLarge]}>Verifikasi Akun</Text>
          <Text
            style={[
              Font.text,
              {color: Colors.placeholder, textAlign: 'center'},
            ]}>
            Kami telah mengirimkan 4 digit kode verifikasi ke{'\n'}{' '}
            <Text style={[Font.text, {color: Colors.primary1}]}>
              {' '}
              {route.params.data.email}{' '}
            </Text>
          </Text>
        </View>
        <View style={{height: 16}} />
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <Text style={[Font.text]}>Masukan Kode</Text>
          <Text
            style={
              isResend
                ? [Font.bold, {color: Colors.primary1}]
                : [Font.bold, {color: Colors.placeholder}]
            }
            onPress={() => (isResend ? resendToken() : false)}>
            Kirim Ulang ({count}d)
          </Text>
        </View>
        <View style={{height: 16}} />
        <OTPInputView
          style={{
            width: '100%',
            height: 85,
          }}
          pinCount={4}
          autoFocusOnLoad
          codeInputFieldStyle={[
            {
              fontSize: 18,
              color: Colors.black,
              fontWeight: 'bold',
              width: 63,
              height: 70,
              // backgroundColor: Colors.gray6,
              borderWidth: 1,
              borderRadius: 7,
            },
            //   success === false
            //     ? {
            //         borderWidth: 1,
            //         borderColor: colors.red,
            //       }
            //     : false,
          ]}
          codeInputHighlightStyle={Colors.primary1}
          onCodeChanged={code => {
            setForm({
              ...form,
              ['otp']: code,
            });
          }}
        />
        <View style={{height: 16}} />
        <BtnPrimary title={'Verifikasi'} pressed={() => submit()} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default OtpVerif;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
