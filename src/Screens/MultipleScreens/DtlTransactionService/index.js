import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Colors from '../../../Styles/Colors';
import TextRows from '../../Compenent/TextRows';
import Font from '../../../Styles/Fonts';
import FormatMoney from '../../Compenent/FormatMoney';
import Horizontal from '../../Compenent/Horizontal';
import BottomService2 from '../../Compenent/BottomService2';

const DtlTransactionService = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView style={{padding: 16}}>
        <Text style={[Font.subtitle]}>Rangkuman Pesanan</Text>
        <View style={{height: 16}} />
        {route.params?.data?.Kategori_Produk ? (
          <TextRows
            text1={'Kategori Produk'}
            text2={route.params?.data?.Kategori_Produk}
          />
        ) : (
          false
        )}
        {/* PDAM */}
        {route.params?.data?.Cluster ? (
          <TextRows text1={'Cluster'} text2={route.params?.data?.Cluster} />
        ) : (
          false
        )}
        {route.params?.data?.Wilayah ? (
          <TextRows text1={'Wilayah'} text2={route.params?.data?.Wilayah} />
        ) : (
          false
        )}
        {route.params?.data?.Nomor_Pelanggan ? (
          <TextRows
            text1={'Nomor Pelanggan'}
            text2={route.params?.data?.Nomor_Pelanggan}
          />
        ) : (
          false
        )}
        {/* PDAM */}
        {/* Listrik PLN */}
        {route.params?.data?.Jenis_Layanan ? (
          <TextRows
            text1={'Jenis Layanan'}
            text2={route.params?.data?.Jenis_Layanan}
          />
        ) : (
          false
        )}
        {route.params?.data?.Tarif_Daya ? (
          <TextRows
            text1={'Tarif / Daya'}
            text2={route.params?.data?.Tarif_Daya}
          />
        ) : (
          false
        )}
        {route.params?.data?.Bulan_Tahun ? (
          <TextRows
            text1={'Bulan / Tahun'}
            text2={route.params?.data?.Bulan_Tahun}
          />
        ) : (
          false
        )}
        {route.params?.data?.ID_Pelanggan ? (
          <TextRows
            text1={'ID Pelanggan'}
            text2={route.params?.data?.ID_Pelanggan}
          />
        ) : (
          false
        )}
        {/* Listrik PLN */}
        {/* kredit */}
        {route.params?.data?.Penyedia_Layanan ? (
          <TextRows
            text1={'Penyedia Layanan'}
            text2={route.params?.data?.Penyedia_Layanan}
          />
        ) : (
          false
        )}
        {route.params?.data?.Tagihan_Ke ? (
          <TextRows
            text1={'Tagihan Ke'}
            text2={route.params?.data?.Tagihan_Ke}
          />
        ) : (
          false
        )}
        {route.params?.data?.Nomor_Kontrak ? (
          <TextRows
            text1={'Nomor Kontrak'}
            text2={route.params?.data?.Nomor_Kontrak}
          />
        ) : (
          false
        )}
        {/* kredit */}
        {/* bpjs */}
        {route.params?.data?.Jenis_BPJS ? (
          <TextRows
            text1={'Jenis BPJS'}
            text2={route.params?.data?.Jenis_BPJS}
          />
        ) : (
          false
        )}
        {route.params?.data?.Bayar_Sampai ? (
          <TextRows
            text1={'Bayar Sampai'}
            text2={route.params?.data?.Bayar_Sampai}
          />
        ) : (
          false
        )}
        {route.params?.data?.Nomor_Virtual_Account ? (
          <TextRows
            text1={'Nomor Virtual Account'}
            text2={route.params?.data?.Nomor_Virtual_Account}
          />
        ) : (
          false
        )}
        {/* bpjs */}
        {route.params?.data?.Pilihan_Produk ? (
          <TextRows
            text1={'Pilihan Produk'}
            text2={route.params?.data?.Pilihan_Produk}
          />
        ) : (
          false
        )}
        {route.params?.data?.Nomor_Telepon ? (
          <TextRows
            text1={'Nomor Telepon'}
            text2={route.params?.data?.Nomor_Telepon}
          />
        ) : (
          false
        )}
        {route.params?.data?.Nama_Pelanggan ? (
          <TextRows
            text1={'Nama Pelanggan'}
            text2={route.params?.data?.Nama_Pelanggan}
          />
        ) : (
          false
        )}
        {route.params?.data?.Tagihan ? (
          <TextRows
            text1={'Tagihan'}
            text2={<FormatMoney value={route.params?.data?.Tagihan} />}
          />
        ) : (
          false
        )}
        {route.params?.data?.Biaya_Admin ? (
          <TextRows
            text1={'Biaya Admin'}
            text2={<FormatMoney value={route.params?.data?.Biaya_Admin} />}
          />
        ) : (
          false
        )}
        <Horizontal />
        <View style={{height: 16}} />
        {route.params?.data?.Total_Pembayaran ? (
          <TextRows
            text1={<Text style={[Font.subtitle]}>Total Pembayaran</Text>}
            text2={
              <FormatMoney
                value={route.params?.data?.Total_Pembayaran}
                style={[Font.subtitle, {color: Colors.error}]}
              />
            }
          />
        ) : (
          false
        )}
      </ScrollView>
      <BottomService2
        balance={'100000000'}
        pressed={() =>
          navigation.navigate('SuccessTransactionService', {
            type: route.params.type,
            data: route.params.data,
          })
        }
      />
    </SafeAreaView>
  );
};

export default DtlTransactionService;

const styles = StyleSheet.create({});
