import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useRef, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Modalize} from 'react-native-modalize';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {screenWidth} from '../../../Styles/Styles';
import {key, getTransactionDetail} from '../../../Variable/ApiClient';
import BtnOutline from '../../Compenent/BtnOutline';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FormatMoney from '../../Compenent/FormatMoney';
import Horizontal from '../../Compenent/Horizontal';

const Product = [
  {
    id: 1,
    title: 'Teh Pucuk Harum 250ml',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 2,
    title: 'Chitato Sapi Panggang 68gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 3,
    title: 'Indomie Mie Goreng Original 85gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 4,
    title: 'Indomie Mie Goreng Cup 75gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
];

const listCancel = [
  {
    id: 1,
    title: 'Ingin mengubah alamat pengiriman',
  },
  {
    id: 2,
    title: 'Ingin mengubah pesanan',
  },
  {
    id: 3,
    title: 'Ingin mengubah metode pembayaran',
  },
  {
    id: 4,
    title: 'Pembayaran terlalu sulit',
  },
  {
    id: 5,
    title: 'Tidak jadi pesan',
  },
];

const DtlTransaksiProduct = ({route}) => {
  const modalCancel = useRef(null);

  const onOpen = () => {
    modalCancel.current?.open();
  };

  const onClose = () => {
    modalCancel.current?.close();
  };

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getDtlHistory(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const getDtlHistory = auth => {
    axios
      .get(getTransactionDetail + '/' + route.params.id, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        return console.log('agagag', res.data);
        res.data.success
          ? setProfile(res.data.data)
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const textRows = (text1, text2, right) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text
        style={[
          Font.bold,
          right === true ? {flex: 1, textAlign: 'right'} : {flex: 1},
        ]}>
        {text2}
      </Text>
    </View>
  );

  const status = param => {
    switch (param) {
      case 'Selesai':
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.successshading,
                color: Colors.success,
                borderRadius: 10,
              },
            ]}>
            Selesai
          </Text>
        );
      case 'Diproses':
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.primary1shading,
                color: Colors.primary1,
                borderRadius: 10,
              },
            ]}>
            Diproses
          </Text>
        );
      case 'Menunggu Pembayaran':
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.yellowshading,
                color: Colors.yellow,
                borderRadius: 10,
              },
            ]}>
            Menunggu Pembayaran
          </Text>
        );
      default:
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.errorshading,
                color: Colors.error,
                borderRadius: 10,
              },
            ]}>
            Gagal
          </Text>
        );
    }
  };

  const renderCancel = ({item}) => (
    <TouchableOpacity>
      <View style={{height: 20}} />
      <View style={styles.wrapDetailOrder}>
        <Text style={Font.subtitle2}>{item.title}</Text>
        <MaterialCommunityIcons
          name="radiobox-blank"
          size={20}
          color={Colors.gray2}
        />
      </View>
      <View style={{height: 12}} />
      <Horizontal />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* header */}
        {/* status pesanan  */}
        <View style={styles.wrapContent}>
          <View style={styles.wrapRow}>
            <Text style={Font.bold}>Status Pesanan</Text>
            {status(route.params)}
          </View>
          <Horizontal />
          <View style={{height: 16}} />
          {textRows('Waktu Pembelian', '12 Des 2021 , 09:20')}
          {textRows(
            'Invoice',
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
              <Text style={Font.bold}>102381082421 </Text>
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>,
          )}
          {route.params === 'Gagal' ? (
            <View
              style={{
                backgroundColor: Colors.errorshading,
                borderWidth: 1,
                borderColor: Colors.error,
                padding: 12,
                flexDirection: 'row',
                borderRadius: 10,
                alignItems: 'center',
              }}>
              <Foundation name="info" size={25} color={Colors.error} />
              <View style={{marginLeft: 10}}>
                <Text style={[Font.bold, {color: Colors.error}]}>
                  Dibatalkan
                </Text>
                <Text style={[Font.text, {color: Colors.placeholder}]}>
                  “Ingin mengubah pesanan”
                </Text>
              </View>
            </View>
          ) : (
            false
          )}
        </View>

        {/* pembayaran */}
        {route.params === 'Gagal' ? (
          false
        ) : (
          <>
            <View style={styles.spaceGrey} />

            <View style={styles.wrapContent}>
              <Text style={[Font.subtitle]}>Pembayaran</Text>
              <View style={{flex: 1, height: 16}} />
              <View style={styles.wrapPayment}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={require('../../../Assets/images/bca.png')}
                    style={{width: 48, height: 16, marginRight: 8}}
                  />
                  <Text style={Font.text}>BCA Virtual Account</Text>
                </View>
                <View style={styles.spacing} />
                {route.params === 'Menunggu Pembayaran' ? (
                  <View>
                    <Horizontal />
                    <View style={styles.spacing} />
                    <View>
                      <Text
                        style={[
                          Font.text,
                          {borderStartColor: Colors.placeholder},
                        ]}>
                        No Virtual Account
                      </Text>
                      <View style={{height: 4}} />
                      <TouchableOpacity
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={Font.title}>32323232332323 </Text>
                        <Feather name="copy" size={18} color={Colors.black} />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.spacing} />
                    <Horizontal />
                    <View style={styles.spacing} />
                    <Text style={[Font.text, {color: Colors.placeholder}]}>
                      Nominal Pembayaran
                    </Text>
                    <View style={{height: 4}} />
                    <TouchableOpacity
                      style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text style={Font.title}>Rp10.000.000 </Text>
                      <Feather name="copy" size={18} color={Colors.black} />
                    </TouchableOpacity>
                    <View style={styles.spacing} />
                    <BtnOutline title={'Cara Pembayaran'} />
                    <View style={styles.spacing} />
                  </View>
                ) : (
                  <View style={styles.wrapVerifPayment}>
                    <AntDesign
                      name="checkcircleo"
                      size={20}
                      color={Colors.success}
                    />
                    <Text
                      style={[
                        Font.bold,
                        {color: Colors.success, marginLeft: 8},
                      ]}>
                      Pembayaran sudah diverifikasi
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </>
        )}

        {/* pengiriman */}
        <View style={styles.spaceGrey} />
        <View style={styles.wrapContent}>
          <Text style={[Font.subtitle]}>Pengiriman</Text>
          <View style={{flex: 1, flexDirection: 'row', marginTop: 16}}>
            <Feather name="map-pin" size={20} color={Colors.primary1} />
            <View style={{flex: 1, marginLeft: 8}}>
              <Text style={[Font.bold]}>Aditiya Permana</Text>
              <Text style={[Font.text, {color: Colors.gray2}]}>
                0812932829320
              </Text>
              <Text style={[Font.text, {color: Colors.gray2}]}>
                Jl. Ciomas Raya Rt01/Rw011 Desa Ciomas Kecamatan Ciomas
              </Text>
            </View>
          </View>

          <View style={{flex: 1, flexDirection: 'row', marginTop: 16}}>
            <MaterialCommunityIcons
              name="truck-fast-outline"
              size={20}
              color={Colors.primary1}
            />
            <View style={{flex: 1, marginLeft: 8}}>
              <Text style={[Font.bold]}>JNE - Reguler</Text>
              <Text style={[Font.text, {color: Colors.gray2}]}>Rp10.000</Text>
              {route.params === 'Diproses' ? (
                false
              ) : (
                <TouchableOpacity
                  style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text style={[Font.text, {color: Colors.gray2}]}>
                    No Resi : 82918918391823{' '}
                  </Text>
                  <Feather name="copy" size={18} color={Colors.gray2} />
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>

        {/* Rangkuman Pesanan */}
        <View style={styles.spaceGrey} />
        <View style={styles.wrapContent}>
          <Text style={[Font.subtitle]}>Rangkuman Pesanan</Text>
          <FlatList
            data={Product}
            renderItem={({item}) => (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginBottom: 16,
                  alignItems: 'center',
                }}>
                <View style={styles.wrapQtyProduct}>
                  <Text style={[Font.bold, {color: Colors.primary1}]}>
                    {item.qty}
                  </Text>
                </View>
                <View style={{flex: 1, marginHorizontal: 16}}>
                  <Text style={[Font.text, {color: Colors.gray1}]}>
                    {item.title}
                  </Text>
                  <Text style={[Font.text, {color: Colors.placeholder}]}>
                    {item.qty} x {item.price}
                  </Text>
                </View>
                <FormatMoney value={item.total} style={[Font.bold]} />
              </View>
            )}
            style={{marginTop: 16}}
          />
          <Text style={[Font.subtitle]}>Rangkuman Pesanan</Text>

          <View style={{height: 16}} />
          {textRows('Sub Total', 'Rp1.200.000', true)}
          {textRows('Ongkos Kirim', 'Rp5.000', true)}
          {textRows(
            'Total Harga',
            <FormatMoney value={1205000} style={[Font.titleLarge]} />,
            true,
          )}
          <Horizontal />
          <View style={{height: 16}} />
          <BtnOutline
            title={'Hubungi Kami'}
            color={Colors.gray2}
            leftIcon={
              <SimpleLineIcons
                name="phone"
                size={18}
                color={Colors.gray2}
                style={{marginRight: 10}}
              />
            }
          />
          <View style={{height: 16}} />
          {route.params === 'Menunggu Pembayaran' ? (
            <BtnPrimary
              title={'Batal'}
              bgColor={Colors.error}
              pressed={() => onOpen()}
            />
          ) : (
            false
          )}
        </View>
      </ScrollView>
      <Modalize ref={modalCancel} handlePosition={'inside'} modalHeight={400}>
        <View style={{padding: 16, flex: 1}}>
          <View style={styles.wrapDetailOrder}>
            <Text style={Font.subtitle}>Alasan Dibatalkan</Text>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={Colors.black}
              onPress={() => onClose(modalCancel)}
            />
          </View>
          <Horizontal />
          {/* <View style={{height: 16}} /> */}
          <FlatList
            // showsVerticalScrollIndicator={false}
            data={listCancel}
            renderItem={renderCancel}
            // ListEmptyComponent={
            //   <View
            //     style={{
            //       flex: 1,
            //       justifyContent: 'center',
            //       alignItems: 'center',
            //     }}>
            //     <Text style={styles.subtitle}>{'tunggu sebentar..'}</Text>
            //   </View>
            // }
          />
        </View>
        <View style={styles.absoluteBottom}>
          <BtnPrimary
            title={'Konfirmasi'}
            pressed={() => onCloseDtlProduct()}
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default DtlTransaksiProduct;

const styles = StyleSheet.create({
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  spaceGrey: {backgroundColor: Colors.container, height: 8},
  spacing: {height: 16},
  wrapContent: {backgroundColor: Colors.white, flex: 1, padding: 16},
  wrapPayment: {
    width: screenWidth - 32,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.container,
    padding: 18,
  },
  wrapQtyProduct: {
    width: 36,
    padding: 8,
    borderWidth: 1,
    borderColor: Colors.gray5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  wrapVerifPayment: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    borderRadius: 8,
    backgroundColor: Colors.successshading,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 72,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
});
