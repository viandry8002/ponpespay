import React, {useRef, useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
  Text,
  ToastAndroid,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FormInput from '../../Compenent/FormInput';
import {Modalize} from 'react-native-modalize';
import {
  key,
  getProvince,
  getCity,
  getSubDistrict,
  getMe,
  postAddress,
} from '../../../Variable/ApiClient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SpinnerLoad from '../../Compenent/SpinnerLoad';
import axios from 'axios';
import Horizontal from '../../Compenent/Horizontal';
import Font from '../../../Styles/Fonts';
import AlertMsg from '../../Compenent/AlertMsg';

const FormAddress = ({route, navigation}) => {
  const modalize = useRef(null);
  const [load, setLoad] = useState(false);
  const [listModal, setListModal] = useState([]);
  const [activeModal, setActiveModal] = useState('');
  const [token, setToken] = useState('');

  const onOpen = modal => {
    modal.current?.open();
  };

  const onClose = modal => {
    modal.current?.close();
  };

  const [form, setForm] = useState({
    // recipient_name: route.params.data.name,
    // recipient_phone: route.params.data.recipient_phone,
    province_id: '',
    province: '',
    city_id: '',
    city: '',
    subdistrict_id: '',
    subdistrict: '',
    address: '',
  });

  const getToken = async () => {
    try {
      const auth = await AsyncStorage.getItem('api_token');
      return route.params.type === 'auth'
        ? setToken(auth)
        : getAddressUser(auth);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  // ListProvince
  const getAddressUser = auth => {
    setToken(auth);
    axios
      .get(getMe, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + auth,
        },
      })
      .then(res => {
        // console.log('aaa', res.data.data);
        setForm({
          ...form,
          ['province_id']: res.data.data.province_id,
          ['province']: res.data.data.province,
          ['city_id']: res.data.data.city_id,
          ['city']: res.data.data.city,
          ['subdistrict_id']: res.data.data.subdistrict_id,
          ['subdistrict']: res.data.data.subdistrict,
          ['address']: res.data.data.address,
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  // ListProvince
  const getAreaProvince = () => {
    axios
      .get(getProvince, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        setListModal(res.data.data);
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  // ListCity
  const getAreaCity = () => {
    axios
      .get(getCity + form.province_id, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        setListModal(res.data.data);
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  // Listsubdistrict
  const getAreasubdistrict = () => {
    axios
      .get(getSubDistrict + form.city_id, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        setListModal(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const openModal = nav => {
    onOpen(modalize);
    setActiveModal(nav);

    if (nav === 'Provinsi') {
      getAreaProvince();
    } else if (nav === 'Kota') {
      getAreaCity();
    } else {
      getAreasubdistrict();
    }
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const onSubmit = () => {
    setLoad(true);
    axios
      .post(postAddress, form, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        res.data.status
          ? route.params.type === 'auth'
            ? (ToastAndroid.show(
                'Data Alamat berhasil di simpan',
                ToastAndroid.SHORT,
              ),
              navigation.navigate('Auth', {type: 'Login'}))
            : (ToastAndroid.show(
                'Data Alamat berhasil di ubah',
                ToastAndroid.SHORT,
              ),
              navigation.navigate('ProfilMenu'))
          : AlertMsg(res.data.message);
        setLoad(false);
      })
      .catch(err => {
        AlertMsg(err.response.data.message);
        console.log(err);
        setLoad(false);
      });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        if (activeModal === 'Provinsi') {
          setForm({
            ...form,
            ['province']: item.province_name,
            ['province_id']: item.province_id,
          });
        } else if (activeModal === 'Kota') {
          setForm({
            ...form,
            ['city']: item.city_name,
            ['city_id']: item.city_id,
          });
        } else {
          setForm({
            ...form,
            ['subdistrict']: item.subdistrict_name,
            ['subdistrict_id']: item.subdistrict_id,
          });
        }
        onClose(modalize);
      }}>
      <View style={{height: 16}} />
      <Text style={Font.bold}>
        {item?.province_name || item?.city_name || item?.subdistrict_name}
      </Text>
      <View style={{height: 12}} />
      <Horizontal />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.wrap}>
      <SpinnerLoad loads={load} />
      <ScrollView showsVerticalScrollIndicator={false} style={{padding: 16}}>
        <FormInput
          title={'Provinsi'}
          placholder={'Pilih provinsi'}
          rightIcon={
            <AntDesign name="down" size={16} color={Colors.placeholder} />
          }
          press={() => openModal('Provinsi')}
          inputfocus={false}
          val={form.province}
        />
        <FormInput
          title={'Kabupaten / Kota'}
          placholder={'Pilih kabupaten/kota'}
          rightIcon={
            <AntDesign name="down" size={16} color={Colors.placeholder} />
          }
          press={() => openModal('Kota')}
          inputfocus={false}
          val={form.city}
        />
        <FormInput
          title={'Kecamatan'}
          placholder={'Pilih kecamatan'}
          rightIcon={
            <AntDesign name="down" size={16} color={Colors.placeholder} />
          }
          press={() => openModal('Kecamatan')}
          inputfocus={false}
          val={form.subdistrict}
        />
        <FormInput
          title={'Alamat Detail'}
          placholder={'Masukan alamat detail'}
          val={form.address}
          change={value => onInputChange(value, 'address')}
        />
        {route.params.type === 'auth' ? (
          <BtnPrimary title={'Simpan Data'} pressed={() => onSubmit()} />
        ) : (
          false
        )}
      </ScrollView>
      {route.params.type === 'change' ? (
        <View style={styles.absoluteBottom}>
          <BtnPrimary title={'Simpan Data'} pressed={() => onSubmit()} />
        </View>
      ) : (
        false
      )}

      {/* modalize area */}
      <Modalize
        ref={modalize}
        handlePosition={'inside'}
        modalHeight={400}
        HeaderComponent={
          <View style={[styles.wrapModalize, {marginTop: 16}]}>
            <View style={styles.wrapDetailOrder}>
              <Text style={Font.subtitle}>Pilih {activeModal}</Text>
              <MaterialCommunityIcons
                name="close"
                size={24}
                color={Colors.black}
                onPress={() => onClose(modalize)}
              />
            </View>
          </View>
        }>
        <Horizontal />
        <View style={styles.wrapModalize}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={listModal}
            renderItem={renderItem}
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default FormAddress;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 72,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
  wrapModalize: {paddingHorizontal: 16},
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
});
