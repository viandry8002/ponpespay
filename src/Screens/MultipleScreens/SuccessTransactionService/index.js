import React, {useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {screenWidth} from '../../../Styles/Styles';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FloatService from '../../Compenent/FloatService';
import FormatMoney from '../../Compenent/FormatMoney';
import Gradient from '../../Compenent/Gradient';
import Horizontal from '../../Compenent/Horizontal';
import TextRows from '../../Compenent/TextRows';

const Product = [
  {
    id: 1,
    title: 'Teh Pucuk Harum 250ml',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 2,
    title: 'Chitato Sapi Panggang 68gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 3,
    title: 'Indomie Mie Goreng Original 85gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
  {
    id: 4,
    title: 'Indomie Mie Goreng Cup 75gr',
    qty: '3',
    price: 100000,
    total: 300000,
  },
];

const SuccessTransactionService = ({route}) => {
  useEffect(() => {}, []);

  const textRows = (text1, text2, right) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text
        style={[
          Font.bold,
          right === true ? {flex: 1, textAlign: 'right'} : {flex: 1},
        ]}>
        {text2}
      </Text>
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Gradient
          style={{height: 112, width: screenWidth, position: 'absolute'}}
        />
        {/* absolute modal */}
        <View style={styles.wrapAbsoluteModal}>
          <Image
            source={require('../../../Assets/icons/success.png')}
            style={styles.imageAbsolute}
            resizeMode={'contain'}
          />
          <Text
            style={[
              Font.titleLarge,
              {
                color: Colors.success,
                textAlign: 'center',
                marginTop: 20,
                marginBottom: 16,
              },
            ]}>
            Transaksi Berhasil Dibuat
          </Text>
          {/* Float service */}
          <FloatService
            icon={route.params.type.icon}
            title={route.params.type.title}
            titleNumber={route.params.type.titleNumber}
            number={route.params.type.number}
          />
          <View style={{height: 16}} />
          {route.params?.data?.Status ? (
            <TextRows
              text1={'Status'}
              text2={
                <View
                  style={{
                    backgroundColor: Colors.primary1shading,
                    borderRadius: 8,
                    padding: 8,
                  }}>
                  <Text style={[Font.bold, {color: Colors.primary1}]}>
                    {route.params?.data?.Status}
                  </Text>
                </View>
              }
            />
          ) : (
            false
          )}
          {route.params?.data?.Waktu_Transaksi ? (
            <TextRows
              text1={'Waktu Transaksi'}
              text2={route.params?.data?.Waktu_Transaksi}
            />
          ) : (
            false
          )}
          {route.params?.data?.Invoice ? (
            <TextRows
              text1={'Invoice'}
              text2={
                <TouchableOpacity style={styles.wrapCopy}>
                  <Text style={Font.bold}>{route.params?.data?.Invoice} </Text>
                  <Feather name="copy" size={18} color={Colors.black} />
                </TouchableOpacity>
              }
            />
          ) : (
            false
          )}
          <Text style={[Font.bold]}>Rangkuman Pesanan</Text>
          <View style={{height: 16}} />
          {route.params?.data?.Metode_Pembayaran ? (
            <TextRows
              text1={'Metode Pembayaran'}
              text2={route.params?.data?.Metode_Pembayaran}
            />
          ) : (
            false
          )}
          <Horizontal />
          <View style={{height: 16}} />
          {route.params?.data?.Kategori_Produk ? (
            <TextRows
              text1={'Kategori Produk'}
              text2={route.params?.data?.Kategori_Produk}
            />
          ) : (
            false
          )}
          {/* PDAM */}
          {route.params?.data?.Cluster ? (
            <TextRows text1={'Cluster'} text2={route.params?.data?.Cluster} />
          ) : (
            false
          )}
          {route.params?.data?.Wilayah ? (
            <TextRows text1={'Wilayah'} text2={route.params?.data?.Wilayah} />
          ) : (
            false
          )}
          {route.params?.data?.Nomor_Pelanggan ? (
            <TextRows
              text1={'Nomor Pelanggan'}
              text2={route.params?.data?.Nomor_Pelanggan}
            />
          ) : (
            false
          )}
          {/* PDAM */}
          {/* Listrik PLN */}
          {route.params?.data?.Jenis_Layanan ? (
            <TextRows
              text1={'Jenis Layanan'}
              text2={route.params?.data?.Jenis_Layanan}
            />
          ) : (
            false
          )}
          {route.params?.data?.Tarif_Daya ? (
            <TextRows
              text1={'Tarif / Daya'}
              text2={route.params?.data?.Tarif_Daya}
            />
          ) : (
            false
          )}
          {route.params?.data?.Bulan_Tahun ? (
            <TextRows
              text1={'Bulan / Tahun'}
              text2={route.params?.data?.Bulan_Tahun}
            />
          ) : (
            false
          )}
          {route.params?.data?.ID_Pelanggan ? (
            <TextRows
              text1={'ID Pelanggan'}
              text2={route.params?.data?.ID_Pelanggan}
            />
          ) : (
            false
          )}
          {/* Listrik PLN */}
          {/* kredit */}
          {route.params?.data?.Penyedia_Layanan ? (
            <TextRows
              text1={'Penyedia Layanan'}
              text2={route.params?.data?.Penyedia_Layanan}
            />
          ) : (
            false
          )}
          {route.params?.data?.Tagihan_Ke ? (
            <TextRows
              text1={'Tagihan Ke'}
              text2={route.params?.data?.Tagihan_Ke}
            />
          ) : (
            false
          )}
          {route.params?.data?.Nomor_Kontrak ? (
            <TextRows
              text1={'Nomor Kontrak'}
              text2={route.params?.data?.Nomor_Kontrak}
            />
          ) : (
            false
          )}
          {/* kredit */}
          {/* bpjs */}
          {route.params?.data?.Jenis_BPJS ? (
            <TextRows
              text1={'Jenis BPJS'}
              text2={route.params?.data?.Jenis_BPJS}
            />
          ) : (
            false
          )}
          {route.params?.data?.Bayar_Sampai ? (
            <TextRows
              text1={'Bayar Sampai'}
              text2={route.params?.data?.Bayar_Sampai}
            />
          ) : (
            false
          )}
          {route.params?.data?.Nomor_Virtual_Account ? (
            <TextRows
              text1={'Nomor Virtual Account'}
              text2={route.params?.data?.Nomor_Virtual_Account}
            />
          ) : (
            false
          )}
          {/* bpjs */}
          {route.params?.data?.Nomor ? (
            <TextRows text1={'Nomor'} text2={route.params?.data?.Nomor} />
          ) : (
            false
          )}
          {route.params?.data?.Harga ? (
            <TextRows
              text1={'Harga'}
              text2={<FormatMoney value={route.params?.data?.Harga} />}
            />
          ) : (
            false
          )}
          {route.params?.data?.Pilihan_Produk ? (
            <TextRows
              text1={'Pilihan Produk'}
              text2={route.params?.data?.Pilihan_Produk}
            />
          ) : (
            false
          )}
          {route.params?.data?.Nomor_Telepon ? (
            <TextRows
              text1={'Nomor Telepon'}
              text2={route.params?.data?.Nomor_Telepon}
            />
          ) : (
            false
          )}
          {route.params?.data?.Nama_Pelanggan ? (
            <TextRows
              text1={'Nama Pelanggan'}
              text2={route.params?.data?.Nama_Pelanggan}
            />
          ) : (
            false
          )}
          {route.params?.data?.Tagihan ? (
            <TextRows
              text1={'Tagihan'}
              text2={<FormatMoney value={route.params?.data?.Tagihan} />}
            />
          ) : (
            false
          )}
          {route.params?.data?.Biaya_Admin ? (
            <TextRows
              text1={'Biaya Admin'}
              text2={<FormatMoney value={route.params?.data?.Biaya_Admin} />}
            />
          ) : (
            false
          )}
          <Horizontal />
          <View style={{height: 16}} />
          {route.params?.data?.Total_Pembayaran ? (
            <TextRows
              text1={<Text style={[Font.subtitle]}>Total Pembayaran</Text>}
              text2={
                <FormatMoney
                  value={route.params?.data?.Total_Pembayaran}
                  style={[Font.subtitle, {color: Colors.error}]}
                />
              }
            />
          ) : (
            false
          )}
        </View>
        <View style={{marginHorizontal: 16}}>
          <BtnPrimary title={'Selanjutnya'} />
        </View>
        {/* Pembayaran */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default SuccessTransactionService;

const styles = StyleSheet.create({
  wrapAbsoluteModal: {
    marginTop: 39,
    marginBottom: 16,
    marginHorizontal: 16,
    borderRadius: 8,
    width: screenWidth - 32,
    padding: 16,
    backgroundColor: Colors.white,
    elevation: 10,
  },
  imageAbsolute: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    position: 'absolute',
    top: -30,
  },
  spacing: {height: 16},
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  wrapCopy: {flexDirection: 'row', alignItems: 'center', flex: 1},
  wrapContent: {backgroundColor: Colors.white, flex: 1},
  wrapPayment: {
    width: screenWidth - 32,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.container,
    paddingVertical: 12,
    paddingHorizontal: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapQtyProduct: {
    width: 36,
    padding: 8,
    borderWidth: 1,
    borderColor: Colors.gray5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 100,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
  rowCenter: {flexDirection: 'row', alignItems: 'center', flex: 1},
});
