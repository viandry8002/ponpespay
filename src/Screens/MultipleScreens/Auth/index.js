import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {ScrollView, StatusBar, StyleSheet, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {key, postLogin, postRegister} from '../../../Variable/ApiClient';
import AlertMsg from '../../Compenent/AlertMsg';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FormInput from '../../Compenent/FormInput';
import SpinnerLoad from '../../Compenent/SpinnerLoad';
import {Dialog} from '@rneui/themed';

const Auth = ({navigation, route}) => {
  const [load, setLoad] = useState(false);
  const [form, setForm] = useState({
    email: '',
    password: '',
  });
  const [formRegist, setFormRegist] = useState({
    name: '',
    no_telp: '',
    email: '',
    password: '',
    password_confirmation: '',
  });
  const [show, setShow] = useState(true);
  const [title, setTitle] = useState('');
  const [subtitle, setSubtitle] = useState('');
  const [visible5, setVisible5] = useState(false);

  useEffect(() => {
    typeTitle();
  }, []);

  const typeTitle = () => {
    route.params.type === 'Login'
      ? (setTitle('Login PonpesPay'), setSubtitle('Masuk dengan akun anda'))
      : (setTitle('Daftar PonpeyPay'), setSubtitle('Membuat akun baru'));
  };

  const onInputChange = (value, input) => {
    route.params.type === 'Login'
      ? setForm({
          ...form,
          [input]: value,
        })
      : setFormRegist({
          ...formRegist,
          [input]: value,
        });
  };

  const saveToken = async token => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    setLoad(true);
    route.params.type === 'Login'
      ? axios
          .post(postLogin, form)
          .then(res => {
            setLoad(false);
            res.data.success
              ? (saveToken(res.data.token),
                navigation.reset({
                  index: 0,
                  routes: [{name: 'Home'}],
                }))
              : AlertMsg(res.data.message);
          })
          .catch(err => {
            // console.log(err.response.data);
            err.response.data.message === 'Anda belum verifikasi akun!'
              ? navigation.navigate('OtpVerif', {
                  data: err.response.data.data,
                  type: 'regist',
                })
              : AlertMsg(err.response.data.message);

            setLoad(false);
          })
      : axios
          .post(postRegister, formRegist)
          .then(res => {
            res.data.status
              ? navigation.navigate('OtpVerif', {
                  data: res.data.data,
                  type: 'regist',
                })
              : false;
            setLoad(false);
          })
          .catch(err => {
            console.log(err);
            setLoad(false);
          });
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="transparent" translucent />
      <LinearGradient
        start={{x: 0.0, y: 0.5}}
        end={{x: 1.0, y: 0.5}}
        colors={[Colors.primary1, Colors.primary2]}
        style={{flex: 1}}>
        <SpinnerLoad loads={load} />
        <View style={{height: 84}} />
        <View style={styles.wrapWelcome}>
          <View style={{flex: 1}}>
            <Text style={[Font.title30Large, {color: Colors.white}]}>
              {title}
            </Text>
            <View style={{height: 6}} />
            <Text style={[Font.subtitle2, {color: Colors.white}]}>
              {subtitle}
            </Text>
          </View>
        </View>
        <View style={styles.wrapForm}>
          {/* login */}
          {route.params.type === 'Login' ? (
            <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
              <FormInput
                title={'Email'}
                placholder={'Masukan email'}
                icon={
                  <MaterialCommunityIcons
                    name="email"
                    size={20}
                    color={Colors.primary1}
                  />
                }
                val={form.email}
                change={value => onInputChange(value, 'email')}
              />
              <FormInput
                title={'Kata Sandi'}
                placholder={'Masukan kata sandi'}
                icon={
                  <Fontisto name="locked" size={20} color={Colors.primary1} />
                }
                rightIcon={
                  <MaterialCommunityIcons
                    onPress={() => setShow(!show)}
                    name={show === true ? 'eye-off-outline' : 'eye-outline'}
                    size={20}
                    color={Colors.black}
                  />
                }
                scureText={show}
                val={form.password}
                change={value => onInputChange(value, 'password')}
              />
              <Text
                style={[
                  Font.bold,
                  {color: Colors.primary1, alignSelf: 'flex-end'},
                ]}
                onPress={() => navigation.navigate('EmailForgot')}>
                Lupa Password?
              </Text>
              <View style={styles.spacing} />
              <BtnPrimary
                title={'Login'}
                pressed={() => submit()}
                // login
                // pressed={() => navigation.navigate('Home')}
              />
              <View style={styles.spacing} />
              <Text style={[Font.text, {alignSelf: 'center'}]}>
                Belum punya akun?{' '}
                <Text
                  style={[Font.bold, {color: Colors.primary1}]}
                  onPress={() => navigation.push('Auth', {type: 'Register'})}>
                  Daftar disini
                </Text>
              </Text>
            </ScrollView>
          ) : (
            <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
              <FormInput
                title={'Nama Lengkap'}
                placholder={'Masukan nama lengkap'}
                val={formRegist.name}
                change={value => onInputChange(value, 'name')}
              />
              <FormInput
                title={'No. Telpon'}
                placholder={'Masukan no telpon'}
                val={formRegist.no_telp}
                change={value => onInputChange(value, 'no_telp')}
                tkeyboard={'numeric'}
              />
              <FormInput
                title={'Email'}
                placholder={'Masukan email'}
                val={formRegist.email}
                change={value => onInputChange(value, 'email')}
              />
              <FormInput
                title={'Password'}
                placholder={'Masukan password'}
                rightIcon={
                  <MaterialCommunityIcons
                    onPress={() => setShow(!show)}
                    name={show === true ? 'eye-off-outline' : 'eye-outline'}
                    size={20}
                    color={Colors.black}
                  />
                }
                scureText={show}
                val={formRegist.password}
                change={value => onInputChange(value, 'password')}
              />
              <FormInput
                title={'Konfirmasi Password'}
                placholder={'Masukan password'}
                rightIcon={
                  <MaterialCommunityIcons
                    onPress={() => setShow(!show)}
                    name={show === true ? 'eye-off-outline' : 'eye-outline'}
                    size={20}
                    color={Colors.black}
                  />
                }
                scureText={show}
                val={formRegist.password_confirmation}
                change={value => onInputChange(value, 'password_confirmation')}
              />
              <Text
                style={[
                  Font.bold,
                  {color: Colors.primary1, alignSelf: 'flex-end'},
                ]}
                onPress={() => navigation.navigate('EmailForgot')}>
                Lupa Password?
              </Text>
              <View style={styles.spacing} />
              <BtnPrimary title={'Daftar Akun'} pressed={() => submit()} />
              <View style={styles.spacing} />
              <Text style={[Font.text, {alignSelf: 'center'}]}>
                Sudah punya akun?{' '}
                <Text
                  style={[Font.bold, {color: Colors.primary1}]}
                  onPress={() => navigation.navigate('Register')}>
                  Login disini
                </Text>
              </Text>
            </ScrollView>
          )}
        </View>
      </LinearGradient>
    </View>
  );
};

export default Auth;

const styles = StyleSheet.create({
  spacing: {
    height: 16,
  },
  wrapWelcome: {height: 119, padding: 16, flexDirection: 'row'},
  wrapForm: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    padding: 16,
  },
});
