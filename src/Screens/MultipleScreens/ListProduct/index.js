import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import React, {useRef, useState, useEffect} from 'react';
import MasonryList from '@react-native-seoul/masonry-list';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import FormatMoney from '../../Compenent/FormatMoney';
import {Modalize} from 'react-native-modalize';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import BtnPrimary from '../../Compenent/BtnPrimary';
import {key, getProducts, getProductsDetail} from '../../../Variable/ApiClient';
import AlertMsg from '../../Compenent/AlertMsg';
import axios from 'axios';

const dataProduk = [
  {
    id: 1,
    file: require('../../../Assets/images/product/pucukHarum.png'),
    title: 'Teh Pucuk Harum 250ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 2,
    file: require('../../../Assets/images/product/chitato.png'),
    title: 'Chitato Sapi Panggang 68gr',
    price: 100000,
    discFrom: null,
    stock: 32,
  },
  {
    id: 3,
    file: require('../../../Assets/images/product/indomie.png'),
    title: 'Indomie Mie Goreng Original 85gr',
    price: 100000,
    discFrom: null,
    stock: 32,
  },
  {
    id: 4,
    file: require('../../../Assets/images/product/indomie_cup.png'),
    title: 'Indomie Mie Goreng Cup 75 Gr',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 5,
    file: require('../../../Assets/images/product/pocari.png'),
    title: 'Pocari Sweat 500ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 6,
    file: require('../../../Assets/images/product/bearbrand.png'),
    title: 'Susu Nestle Bear Brand  189ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 7,
    file: require('../../../Assets/images/product/pucukHarum.png'),
    title: 'Teh Pucuk Harum 250ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 8,
    file: require('../../../Assets/images/product/pucukHarum.png'),
    title: 'Teh Pucuk Harum 250ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 9,
    file: require('../../../Assets/images/product/pucukHarum.png'),
    title: 'Teh Pucuk Harum 250ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
  {
    id: 10,
    file: require('../../../Assets/images/product/pucukHarum.png'),
    title: 'Teh Pucuk Harum 250ml',
    price: 100000,
    discFrom: 200000,
    stock: 32,
  },
];

const ListProduct = ({dataList, onRefresh, refresh}) => {
  // console.log('hhhhhhhhh', route.params.dataList);
  const [choseProduct, setChoseProduct] = useState('');

  const modalDtlProduct = useRef(null);

  const onOpenDtlProduct = () => {
    modalDtlProduct.current?.open();
  };

  const onCloseDtlProduct = () => {
    modalDtlProduct.current?.close();
  };

  useEffect(() => {
    // getListProduct();
  }, []);

  // const getListProduct = () => {
  //   // get product
  //   axios
  //     .get(
  //       getProducts +
  //         '?category_id=' +
  //         route.params.id +
  //         '/?q=' +
  //         route.params.search,
  //     )
  //     .then(res => {
  //       res.data.status ? setProduct(res.data.data) : false;
  //     })
  //     .catch(err => {
  //       console.log(err);
  //     });
  // };

  return (
    <SafeAreaView style={{flex: 1}}>
      <MasonryList
        showsVerticalScrollIndicator={false}
        data={dataList}
        // data={product}
        renderItem={({item}) => (
          <TouchableOpacity
            style={{
              margin: 4,
              borderRadius: 10,
              backgroundColor: Colors.white,
            }}
            onPress={() => {
              setChoseProduct(item);
              onOpenDtlProduct();
            }}>
            <Image
              source={{uri: item.file}}
              // source={item.file}
              resizeMode="contain"
              style={{
                width: '100%',
                height: 130,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
              }}
            />
            <View style={{flex: 1, margin: 8}}>
              <Text style={Font.text}>{item.title}</Text>
              <FormatMoney value={item.price_result} style={Font.bold} />
              {item.discount != '' ? (
                <FormatMoney
                  value={Number(item.discount)}
                  style={[
                    Font.caption,
                    {
                      color: Colors.placeholder,
                      textDecorationLine: 'line-through',
                    },
                  ]}
                />
              ) : (
                false
              )}
              <Text style={Font.text}>stock : {item.stock}</Text>
            </View>
          </TouchableOpacity>
        )}
        numColumns={2}
        style={{padding: 4}}
        refreshing={refresh}
        onRefresh={onRefresh}
      />
      {/* btn cart */}
      {/* <View style={styles.absoluteBottom}>
        <BtnPrimary
          customText={
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingHorizontal: 16,
                alignItems: 'center',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Ionicons name="cart" size={24} color={Colors.white} />
                <Text style={[Font.text, {color: Colors.white, marginLeft: 8}]}>
                  Keranjang :
                </Text>
              </View>
              <Text style={[Font.bold, {color: Colors.white}]}>
                2 Item • <FormatMoney value={'200000'} />
              </Text>
            </View>
          }
          pressed={() => navigation.navigate('Checkout')}
        />
      </View> */}
      {/* modalize Product */}
      <Modalize ref={modalDtlProduct} withHandle={false} modalHeight={530}>
        <View style={{borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
          <View style={styles.wrapDetailOrder}>
            <Image
              source={{uri: choseProduct.file}}
              style={styles.imgeModalize}
              resizeMode="contain"
            />
            <TouchableOpacity
              style={styles.wrapCloseModal}
              onPress={() => onCloseDtlProduct()}>
              <AntDesign name="close" size={16} color={Colors.black} />
            </TouchableOpacity>
            {/* {data} */}
          </View>
          <View style={{flex: 1, margin: 16}}>
            {/* detail */}
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={[Font.bold]}>{choseProduct.title}</Text>
                <Text style={[Font.text]}>Stok : {choseProduct.stock}</Text>
              </View>
              <View>
                <FormatMoney
                  value={choseProduct.price_result}
                  style={Font.subtitle}
                />
                {choseProduct.discount != '' ? (
                  <FormatMoney
                    value={Number(choseProduct.discount)}
                    style={[
                      Font.caption,
                      {
                        color: Colors.placeholder,
                        textDecorationLine: 'line-through',
                      },
                    ]}
                  />
                ) : (
                  false
                )}
              </View>
            </View>
            <View style={{height: 32}} />
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              <AntDesign name="minuscircle" size={36} color={Colors.primary1} />
              <Text style={[Font.text, {marginHorizontal: 16}]}>1</Text>
              <AntDesign name="pluscircle" size={36} color={Colors.primary1} />
            </View>
            <View style={{height: 32}} />
          </View>
          <View style={styles.absoluteBottom}>
            <BtnPrimary
              customText={
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 16,
                  }}>
                  <Text style={[Font.text, {color: Colors.white}]}>
                    Keranjang :
                  </Text>
                  <Text style={[Font.bold, {color: Colors.white}]}>
                    2 Item • <FormatMoney value={'200000'} />
                  </Text>
                </View>
              }
              pressed={() => onCloseDtlProduct()}
            />
          </View>
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default ListProduct;

const styles = StyleSheet.create({
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // marginVertical: 8,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 72,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
  imgeModalize: {
    width: '100%',
    height: 300,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    resizeMode: 'contain',
  },
  wrapCloseModal: {
    backgroundColor: Colors.whiteShading,
    padding: 8,
    borderRadius: 100,
    position: 'absolute',
    top: 16,
    right: 16,
  },
});
