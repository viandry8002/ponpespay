import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState, useRef} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
// import Horizontal from '../../Compenent/Horizontal';
// Horizontal
import Feather from 'react-native-vector-icons/Feather';
import Foundation from 'react-native-vector-icons/Foundation';
import {TextMask} from 'react-native-masked-text';
import {Modalize} from 'react-native-modalize';
import Horizontal from '../../Compenent/Horizontal';

const DtlTransaksi = ({route}) => {
  const modalCancel = useRef(null);

  const onOpen = () => {
    modalCancel.current?.open();
  };

  const onClose = () => {
    modalCancel.current?.close();
  };

  const textRows = (text1, text2) => (
    <View style={styles.wrapRow}>
      <Text style={[Font.text, {color: Colors.placeholder, flex: 1}]}>
        {text1}
      </Text>
      <Text style={[Font.bold, {flex: 1}]}>{text2}</Text>
    </View>
  );

  const status = param => {
    switch (param) {
      case 'Selesai':
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.successshading,
                color: Colors.success,
                borderRadius: 10,
              },
            ]}>
            Selesai
          </Text>
        );
      case 'Diproses':
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.primary1shading,
                color: Colors.primary1,
                borderRadius: 10,
              },
            ]}>
            Diproses
          </Text>
        );
      default:
        return (
          <Text
            style={[
              Font.bold,
              {
                padding: 8,
                backgroundColor: Colors.errorshading,
                color: Colors.error,
                borderRadius: 10,
              },
            ]}>
            Gagal
          </Text>
        );
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* header */}
        <View style={{backgroundColor: Colors.white, flex: 1, padding: 16}}>
          <View style={styles.wrapRow}>
            <Text style={Font.bold}>Status Pesanan</Text>
            {status(route.params)}
          </View>
          <Horizontal />
          <View style={{height: 16}} />
          {textRows('Waktu Pembelian', '12 Des 2021 , 09:20')}
          {textRows(
            'Invoice',
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
              <Text style={Font.bold}>102381082421 </Text>
              <Feather name="copy" size={18} color={Colors.black} />
            </TouchableOpacity>,
          )}

          {route.params === 'Gagal' ? (
            <View
              style={{
                backgroundColor: Colors.errorshading,
                borderWidth: 1,
                borderColor: Colors.error,
                padding: 12,
                flexDirection: 'row',
                borderRadius: 10,
                alignItems: 'center',
              }}>
              <Foundation name="info" size={25} color={Colors.error} />
              <Text style={[Font.text, {marginLeft: 10}]}>
                Sedang Terjadi Gangguan, sehingga transaksi tidak bisa
                dilanjutkan
              </Text>
            </View>
          ) : (
            false
          )}
        </View>

        <View style={{backgroundColor: Colors.container, height: 8}} />
        {/* body */}
        <View style={{backgroundColor: Colors.white, flex: 1, padding: 16}}>
          <Text style={[Font.bold, {marginBottom: 16}]}>Rangkuman Pesanan</Text>
          {textRows('Metode Pembayaran', 'Saldo Dompet')}
          <Horizontal />
          <View style={{height: 16}} />
          {textRows('Kategori Produk', 'Pulsa')}
          {textRows('Jenis Layanan', 'Telkomsel 5.000')}
          {textRows('Nomor', '081231231412')}
          {textRows(
            'Harga',
            <TextMask
              type={'money'}
              options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp ',
                suffixUnit: '',
              }}
              value={5850}
              numberOfLines={1}
            />,
          )}
          <Horizontal />
          <View style={{height: 16}} />
          {textRows(
            <Text style={[Font.bold, {flex: 1}]}>Total Pembayaran</Text>,
            <TextMask
              type={'money'}
              options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp ',
                suffixUnit: '',
              }}
              value={5850}
              style={[Font.subtitle, {flex: 1, color: Colors.error}]}
              numberOfLines={1}
            />,
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DtlTransaksi;

const styles = StyleSheet.create({
  wrapRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
});
