import React, {useState, useEffect} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Compenent/BtnPrimary';
import FormInput from '../../Compenent/FormInput';
import {
  key,
  postResetPassword,
  postUpdatePassword,
} from '../../../Variable/ApiClient';
import axios from 'axios';
import AlertMsg from '../../Compenent/AlertMsg';
import SpinnerLoad from '../../Compenent/SpinnerLoad';
import ToastMsg from '../../Compenent/ToastMsg';
import AsyncStorage from '@react-native-async-storage/async-storage';

const FormPassword = ({route, navigation}) => {
  const [load, setLoad] = useState(false);
  const [form, setForm] = useState({
    email: '',
    otp: '',
    password_old: '',
    password: '',
    password_confirmation: '',
  });
  const [visibleP1, setvisibleP1] = useState(true);
  const [visibleP2, setvisibleP2] = useState(true);
  const [visibleP3, setvisibleP3] = useState(true);
  const [token, setToken] = useState('');

  const getToken = async () => {
    try {
      const auth = await AsyncStorage.getItem('api_token');
      return setToken(auth);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
    route.params.type === 'change'
      ? false
      : setForm({
          ...form,
          ['email']: route.params.email,
          ['otp']: route.params.otp,
        });
  }, []);

  const submit = () => {
    setLoad(true);
    axios
      .post(postResetPassword, form, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        res.data.status
          ? navigation.navigate('Success')
          : AlertMsg(res.data.message);
        setLoad(false);
      })
      .catch(err => {
        console.log(err);
        setLoad(false);
      });
  };

  const submitChange = () => {
    setLoad(true);
    axios
      .post(postUpdatePassword, form, {
        headers: {
          // key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        res.data.status
          ? (ToastMsg('sukses merubah data'), navigation.navigate('ProfilMenu'))
          : AlertMsg(res.data.message);
        setLoad(false);
      })
      .catch(err => {
        console.log(err);
        setLoad(false);
      });
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  return (
    <SafeAreaView style={styles.wrap}>
      <SpinnerLoad loads={load} />
      <ScrollView showsVerticalScrollIndicator={false} style={{padding: 16}}>
        {route.params.type === 'change' ? (
          <FormInput
            title={'Password Lama'}
            placholder={'Masukan password'}
            rightIcon={
              <MaterialCommunityIcons
                onPress={() => setvisibleP1(!visibleP1)}
                name={'eye-off-outline'}
                size={20}
                color={Colors.black}
              />
            }
            scureText={visibleP1}
            val={form.password_old}
            change={value => onInputChange(value, 'password_old')}
          />
        ) : (
          false
        )}
        <FormInput
          title={'Password Baru'}
          placholder={'Masukan password'}
          rightIcon={
            <MaterialCommunityIcons
              onPress={() => setvisibleP2(!visibleP2)}
              name={'eye-off-outline'}
              size={20}
              color={Colors.black}
            />
          }
          scureText={visibleP2}
          val={form.password}
          change={value => onInputChange(value, 'password')}
        />
        <FormInput
          title={'Konfirmasi Password'}
          placholder={'Masukan password'}
          rightIcon={
            <MaterialCommunityIcons
              onPress={() => setvisibleP3(!visibleP3)}
              name={'eye-off-outline'}
              size={20}
              color={Colors.black}
            />
          }
          scureText={visibleP3}
          val={form.password_confirmation}
          change={value => onInputChange(value, 'password_confirmation')}
        />
        {route.params.type === 'auth' ? (
          <BtnPrimary title={'Konfirmasi'} pressed={() => submit()} />
        ) : (
          false
        )}
      </ScrollView>
      {route.params.type === 'change' ? (
        <View style={styles.absoluteBottom}>
          <BtnPrimary
            title={'Konfirmasi'}
            // pressed={() => navigation.navigate('OtpVerif', {type: 'forgot'})}
            pressed={() => submitChange()}
          />
        </View>
      ) : (
        false
      )}
    </SafeAreaView>
  );
};

export default FormPassword;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: Colors.white,
    // padding: 16,
  },
  absoluteBottom: {
    backgroundColor: Colors.white,
    height: 72,
    paddingHorizontal: 16,
    justifyContent: 'center',
    elevation: 10,
  },
});
